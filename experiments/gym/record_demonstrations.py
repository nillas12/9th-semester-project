from typing import Tuple

import gym
import numpy as np
from make_env import make_env
import gym.utils.play as play
import argparse
import os
from PIL import Image


def record_demonstrations(env_name, fps, zoom) -> Tuple[np.ndarray, ...]:
    player_env = gym.make(env_name)
    recording_env = make_env(env_name)

    states = []
    actions = []
    rewards = []
    new_states = []
    dones = []

    frame = 0
    frame_skip = 4
    player_env.reset()
    observation = recording_env.reset()
    recording_env.unwrapped.restore_full_state(player_env.unwrapped.clone_full_state())

    def record_transition(_o, _o_, act, _r, _d, _i):
        nonlocal frame, observation
        if frame % frame_skip == 0:
            new_observation, reward, done, _ = recording_env.step(act)
            states.append(np.array(observation))
            actions.append(act)
            rewards.append(reward)
            new_states.append(np.array(new_observation))
            dones.append(done)
            observation = new_observation
            if done:
                observation = recording_env.reset()
            recording_env.unwrapped.restore_full_state(player_env.unwrapped.clone_full_state())
        frame += 1

    play.play(player_env, fps=fps, zoom=zoom, callback=record_transition)
    recording_env.close()
    player_env.close()

    states = np.array(states)
    actions = np.array(actions)
    rewards = np.array(rewards)
    new_states = np.array(new_states)
    dones = np.array(dones)

    return states, actions, rewards, new_states, dones


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Record a good ol\' session of gaming.')
    parser.add_argument('--env', type=str, default='BreakoutNoFrameskip-v4', help='The environment to record.')
    parser.add_argument('--zoom', type=float, default=3.0, help='Upscale the window. --zoom 1 is pixel perfect.')
    parser.add_argument('--fps', type=int, default=30, help='The framerate of the game.')
    args = parser.parse_args()
    demo_folder = 'demonstrations'
    filepath = os.path.join(demo_folder, args.env)+'.npz'

    states, actions, rewards, new_states, dones = record_demonstrations(args.env, args.fps, args.zoom)
    new_n = len(states)
    hej = [Image.fromarray(s[0]) for s in states]
    start = hej[0]
    start.save("out.gif", save_all=True, loop=0, append_images=hej[1:], duration=10)

    answer = 'a'
    if os.path.exists(filepath):
        old_values = np.load(filepath)
        while answer != 'y' and answer != 'n':
            answer = input('Was that good enough that you want to save it? [y/n] >>> ')
            if answer == 'y':
                print('Appending values to old file')
                states = np.concatenate((old_values['states'], states))
                actions = np.concatenate((old_values['actions'], actions))
                rewards = np.concatenate((old_values['rewards'], rewards))
                new_states = np.concatenate((old_values['new_states'], new_states))
                dones = np.concatenate((old_values['dones'], dones))
            elif answer == 'n':
                print('Discarding new recording')
                states = old_values['states']

    total_n = len(states)

    print('Number of transitions recorded: ', new_n)
    print('Number of transitions saved in file: ',  total_n)
    if not os.path.exists(demo_folder):
        os.mkdir(demo_folder)
    if answer != 'n':
        np.savez_compressed(filepath,
                            states=states,
                            actions=actions,
                            rewards=rewards,
                            new_states=new_states,
                            dones=dones)
    print('Saved demonstration to \'', filepath, '\'')
