from PIL import Image


class GifBuilder:
    def __init__(self, name, fps=30, frame_skip=1):
        self.frames = []
        self.count = 0
        self.start = 0
        self.name = name
        self.fps = fps
        self.frame_skip = frame_skip

    def append(self, img, done):
        img = Image.fromarray(img)
        if self.count == 0:
            self.start = img
        elif self.count % self.frame_skip == 0:
            self.frames.append(img)
        self.count += 1
        if done:
            self.save_gif()

    def save_gif(self):
        self.start.save(f"{self.name}.gif",
                        save_all=True,
                        append_images=self.frames,
                        loop=0,
                        duration=1000 / (self.fps / self.frame_skip))
