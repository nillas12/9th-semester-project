from collections import namedtuple

import numpy as np
import os
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.models import load_model
import tensorflow.keras.optimizers as optimizers
from algorithms import ReplayBuffer
import math
import random

from algorithm import Algorithm, SarsAlgorithm

# Needed for tensorflow to work with lots of RAM:
from config import DqnConfig
from.reward_functions import reward_functions

if not os.environ.get('IN_DOCKER', False):
    physical_devices = tf.config.experimental.list_physical_devices('GPU')
    if len(physical_devices):
        tf.config.experimental.set_memory_growth(physical_devices[0], True)


conv2d_params = namedtuple('conv2d_params',
                           ('layer_1', 'layer_2', 'layer_3'))


def build_simple_dqn(input_dims, n_actions, config: DqnConfig):
    """Builds a DQN according to description from the paper."""
    out_channels = conv2d_params(*config.out_channels)
    kernel_sizes = conv2d_params(*config.kernel_sizes)
    strides = conv2d_params(*config.strides)
    fc_size = config.fully_connected_size

    model = keras.Sequential([
        tf.keras.layers.Input(input_dims),
        keras.layers.Conv2D(filters=out_channels.layer_1,
                            kernel_size=kernel_sizes.layer_1,
                            strides=strides.layer_1,
                            activation='relu',
                            name='conv_1',
                            padding='same'),
        keras.layers.Conv2D(filters=out_channels.layer_2,
                            kernel_size=kernel_sizes.layer_2,
                            strides=strides.layer_2,
                            activation='relu',
                            name='conv_2',
                            padding='same'),
        keras.layers.Conv2D(filters=out_channels.layer_3,
                            kernel_size=kernel_sizes.layer_3,
                            strides=strides.layer_3,
                            activation='relu',
                            name='conv_3',
                            padding='same'),
        keras.layers.Flatten(),
        keras.layers.Dense(units=fc_size, activation='relu', name='dense_1'),
        keras.layers.Dense(n_actions, activation=None, name='output')
    ])
    model.compile(optimizer=optimizers.RMSprop(learning_rate=config.learning_rate,
                                               momentum=config.rms_prop_momentum,
                                               decay=config.rms_prop_decay,
                                               epsilon=config.rms_prop_eps,
                                               clipvalue=config.gradient_clip_value),
                  loss=config.loss_function)

    return model


class DqnAlgorithm(SarsAlgorithm):
    """
    DQN algorithm designed after the specs in the DQN paper:
    'Human-level control through deep reinforcement learning'
    """
    epoch_interval = 0.5

    def __init__(self,
                 action_space,
                 input_dims,
                 config: DqnConfig):
        super().__init__(action_space)
        self.action_space = [i for i in range(self.action_space.n)]
        self.gamma = config.gamma
        self.epsilon_start = config.epsilon_start
        self.epsilon_dec = config.epsilon_decay
        self.epsilon_min = config.epsilon_end
        self.batch_size = config.batch_size
        self.memory = ReplayBuffer(config.memory_size, input_dims)
        self.q_network = build_simple_dqn(input_dims, action_space.n, config)
        self.q_target = tf.keras.models.clone_model(self.q_network)
        self.update_target_network()
        self.iterations = 0
        self.gradient_updates = 0
        self.replay_start_size = config.warm_start_size
        self.target_update_interval = config.target_update_interval
        self.gradient_update_interval = config.gradient_update_interval
        self.reward_function = reward_functions[config.reward_function]
        self.epsilon_eval = config.epsilon_eval
        self.updates_in_epoch_step = 0

    def save(self, path):
        self.q_network.save(path)
        # Consider what else we may need to save

    def load(self, path) -> Algorithm:
        self.q_network = load_model(path)
        self.q_target = load_model(path)
        # Consider what else we may need to load
        return self

    def get_action(self, observation, reward, train=True):
        action = self._choose_action(observation, train)
        self.iterations += 1

        if train and \
                self.iterations > self.replay_start_size and \
                self.iterations % self.gradient_update_interval == 0:
            self._learn()
            self.gradient_updates += 1

            if self.gradient_updates % self.target_update_interval == 0:
                self.update_target_network()

        return action

    def _choose_action(self, observation, train=True):
        """Chooses an action based on observation."""
        eps_threshold = self.epsilon_eval if not train else \
            self.epsilon_min + (self.epsilon_start - self.epsilon_min) * \
            math.exp(-1. * self.iterations / self.epsilon_dec)

        if random.random() > eps_threshold:
            state = np.array([observation])
            action = np.argmax(self.q_network.predict(state))
        else:
            action = np.random.choice(self.action_space)

        return action

    def store_transition(self, state, action, reward, new_state, done):
        self.memory.store_transition(state, action, reward, new_state, done)

    def _learn(self):
        """Performs one gradient update."""
        if self.memory.mem_counter < self.batch_size:
            return

        self.updates_in_epoch_step += 1

        states, actions, rewards, new_states, dones = self.memory.sample_buffer(self.batch_size)

        rewards = self.reward_function(rewards)

        state_action_values = self.q_network.predict(states)
        next_state_values = self.q_target.predict(new_states)

        target_values = np.copy(state_action_values)
        batch_index = np.arange(self.batch_size, dtype=np.int32)

        target_values[batch_index, actions] = rewards + self.gamma * np.max(next_state_values, axis=1) * dones

        self.q_network.train_on_batch(states, target_values)

    def update_target_network(self):
        self.q_target.set_weights(self.q_network.get_weights())

    def check_reset_epoch(self):
        if self.updates_in_epoch_step >= 25_000:
            self.updates_in_epoch_step = 0
            return True
        return False
