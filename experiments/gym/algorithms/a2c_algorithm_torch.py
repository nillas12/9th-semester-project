import torch
import torch.optim as optim
from algorithm import Algorithm
from algorithms.a2c_logic.a2c.model import ActorCritic
from algorithms.a2c_logic.train_atari import compute_returns
from config.a2c_config import A2cConfig

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


class A2cTorchAlgorithm(Algorithm):
    epoch_interval = 0.5

    def __init__(self,
                 action_space,
                 observation_space,
                 config: A2cConfig):
        super().__init__(action_space)
        self.actor_critic = ActorCritic(observation_space, action_space).to(device)
        self.gamma = config.gamma
        self.learning_rate = config.learning_rate
        self.rms_prop_alpha = config.rms_prop_alpha
        self.rms_prop_eps = config.rms_prop_eps
        self.actor_loss_coefficient = config.actor_loss_coefficient
        self.critic_loss_coefficient = config.critic_loss_coefficient
        self.entropy_loss_coefficient = config.entropy_loss_coefficient
        self.max_grad_norm = config.max_grad_norm
        self.optimizer = optim.RMSprop(self.actor_critic.parameters(), self.learning_rate)
        self.updates_per_epoch = config.updates_per_epoch
        self.actor_loss = 0
        self.critic_loss = 0
        self.entropy_loss = 0

    def step_actor_critic(self, observation, reward, train=True):
        actor, critic = self.actor_critic(observation)
        return actor, critic

    def step(self, observation, reward, train=True):
        actor, _ = self.actor_critic(observation)
        action = actor.sample()
        return action

    def populate(self, env):
        pass

    def learn(self, next_observation, rewards, masks, log_probs, values, entropies):
        self.updates_in_epoch_step += 1
        next_observation = next_observation.to(device).float() / 255
        with torch.no_grad():
            _, next_values = self.actor_critic(next_observation)
            returns = compute_returns(next_values.squeeze(), rewards, masks, self.gamma)
            returns = torch.cat(returns)

        log_probs = torch.cat(log_probs)
        values = torch.cat(values)
        entropies = torch.cat(entropies)

        advantages = returns - values

        self.actor_loss = actor_loss = -(log_probs * advantages.detach()).mean()
        self.critic_loss = critic_loss = advantages.pow(2).mean()
        self.entropy_loss = entropy_loss = entropies.mean()

        loss = self.actor_loss_coefficient * actor_loss + \
            self.critic_loss_coefficient * critic_loss - \
            self.entropy_loss_coefficient * entropy_loss

        self.backward(loss)

    def backward(self, loss):
        self.optimizer.zero_grad()
        loss.backward()
        torch.nn.utils.clip_grad_norm_(self.actor_critic.parameters(), self.max_grad_norm)
        self.optimizer.step()

    def save(self, path):
        torch.save({
            'ac_state': self.actor_critic.state_dict(),
            'optimizer_state': self.optimizer.state_dict()
        }, path)

    def load(self, path) -> Algorithm:
        data = torch.load(path)
        self.actor_critic.load_state_dict(data['ac_state'])
        self.optimizer.load_state_dict(data['optimizer_state'])
        return self

    def episode_ended(self, observation, reward):
        pass
