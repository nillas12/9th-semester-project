from functools import reduce
from typing import Dict
import numpy as np

import torch
import torch.optim as optim

from algorithms.replay_buffer_numpy import PrioritizedReplayBufferWithDemonstrations
from config import DqfdConfig
from .d3qn_algorithm_torch import DuelingDdqnTorchAlgorithm

cuda = 'cuda'
cpu = 'cpu'
device = torch.device(cuda if torch.cuda.is_available() else cpu)


class DqfdAlgorithm(DuelingDdqnTorchAlgorithm):
    epoch_interval = 0.2

    def __init__(self, demonstrations: Dict[str, np.ndarray], action_space, input_dims, config: DqfdConfig):
        super().__init__(action_space, input_dims, config, 'do_not_make_replay_buffer')

        demonstrations['rewards'] = self.reward_function(demonstrations['rewards'])

        self.n_steps = config.n_steps
        self.batch_size = config.batch_size
        self.alpha = config.alpha
        self.beta = config.beta
        self.beta_inc = config.beta_inc
        self.eps_a = config.eps_a
        self.eps_d = config.eps_d
        self.pre_training_steps = config.pre_training_steps
        self.pre_trained_steps = 0
        self.large_margin_term = config.large_margin_term
        self.lambda_1 = config.lambda_1
        self.lambda_2 = config.lambda_2
        self.lambda_3 = config.l2_regularization_term

        self.memory = PrioritizedReplayBufferWithDemonstrations(
            demonstrations,
            config.memory_size,
            input_dims,
            self.alpha,
            self.eps_a,
            self.eps_d)

        self.optimizer = optim.RMSprop(self.policy_net.parameters(),
                                       lr=config.learning_rate,
                                       weight_decay=self.lambda_3)

    @property
    def pre_training(self):
        return self.memory.pre_training

    def pre_train(self):
        for _ in range(self.pre_training_steps):
            self._optimize_model()
        self.memory.pre_training_done()

    def pre_train_1000_steps(self):
        self.pre_trained_steps += 1_000
        for _ in range(1_000):
            self._optimize_model()
        if self.pre_trained_steps >= self.pre_training_steps:
            self.memory.pre_training_done()

    def populate(self, env):
        self.pre_train()

    def _optimize_model(self):
        if not self.pre_training:
            self.updates_in_epoch_step += 1

        beta = min(1.0, self.beta + self.gradient_updates * (1.0 - self.beta) / self.beta_inc)
        sample = self.memory.sample_buffer(self.batch_size, beta)
        loss = self.calculate_loss(sample)
        self.backward(loss)

    def backward(self, loss):
        self.optimizer.zero_grad()
        loss.backward()
        self.optimizer.step()

    def calculate_loss(self, sample):
        states, actions, rewards, new_states, dones, mem_indices, weights = sample
        states = torch.tensor(states, device=device)
        actions = torch.tensor(actions, dtype=torch.long, device=device)
        rewards = torch.tensor(rewards, device=device)
        new_states = torch.tensor(new_states, device=device)
        dones = torch.tensor(dones, device=device)
        weights = torch.tensor(weights, device=device)

        q_pred = self.policy_net.forward(states)

        td_error, single_loss = self._1_step_loss(q_pred.clone(), actions, rewards, new_states, dones, weights)
        large_margin_loss = self._large_margin_loss(q_pred.clone(), mem_indices, actions)
        n_loss = self._n_step_loss(q_pred.clone(), mem_indices)
        self.memory.update_priorities(mem_indices, td_error)

        return (single_loss + self.lambda_1 * n_loss + self.lambda_2 * large_margin_loss).to(device)

    def _large_margin_loss(self, q_pred, batch_indices, demo_actions):
        loss = []
        demo_idx = np.array([self.memory.is_demonstration(i) for i in batch_indices])
        if demo_idx.any():
            q_pred = q_pred[demo_idx] + self.large_margin_term  # margin = 0.8 if a != aE
            expert_actions = demo_actions[demo_idx]
            indices = torch.arange(len(q_pred))
            q_pred[indices, expert_actions] -= self.large_margin_term  # margin = 0.0 if a == aE
            agent_actions = torch.argmax(q_pred, dim=1)

            for i in range(len(agent_actions)):
                a_agent = agent_actions[i]
                a_expert = expert_actions[i]
                q_expert = q_pred[i, a_expert]
                q_agent = q_pred[i, a_agent]
                loss.append(q_agent - q_expert)
        if len(loss) == 0:  # tensor.mean() returns nan if tensor is empty
            loss = [0.0]
        return torch.tensor(loss, device=device, dtype=torch.float32, requires_grad=True).mean()

    def _1_step_loss(self, q_pred, actions, rewards, new_states, dones, weights):
        indices = torch.arange(self.batch_size)
        q_pred = q_pred[indices, actions]
        q_next = self.target_net.forward(new_states)
        q_eval = self.policy_net.forward(new_states)
        max_actions = torch.argmax(q_eval, dim=1)

        q_next[dones] = 0.0
        q_target = rewards + self.gamma * q_next[indices, max_actions]

        loss = (self.loss_function(q_pred, q_target, reduction='none') * weights).to(device)
        td_error = np.abs((q_pred - q_target).data.cpu().numpy())

        return td_error, loss.mean()

    def _n_step_loss(self, q_pred, indices):
        (rewards_sequences,
         new_states,
         dones,
         state_mask) = self.memory.get_n_demo_steps(self.n_steps, indices)
        n_demos = len(dones)
        if n_demos == 0:
            return torch.tensor(0.0, device=device, dtype=torch.float32, requires_grad=True)

        indices = torch.arange(n_demos)
        new_states = torch.tensor(new_states, device=device)
        dones = torch.tensor(dones, device=device)

        discounted_r = torch.tensor([reduce(self.__sum_rewards, r_seq, (0, 0))[0] for r_seq in rewards_sequences],
                                    device=device)

        q_pred = q_pred[state_mask]
        q_next = self.target_net.forward(new_states)
        max_actions = torch.argmax(q_pred, dim=1)
        q_pred = q_pred[indices, max_actions]

        q_next[dones] = 0.0
        q_target = discounted_r + self.gamma ** self.n_steps * q_next[indices, max_actions]

        return self.loss_function(q_pred, q_target).to(device)

    def __sum_rewards(self, acc, x):
        acc_reward, exp = acc
        return acc_reward + (self.gamma ** exp * x), exp + 1
