import numpy as np


def reward_sign(rewards):
    return np.sign(rewards)


def reward_mean_stddev(rewards):
    r = rewards - np.mean(rewards)
    r /= np.std(r)
    return r


def reward_log(rewards):
    return np.sign(rewards) * np.log(1 + np.abs(rewards))


def reward_no_change(rewards):
    return rewards


reward_functions = {
    'sign': reward_sign,
    'mean_stddev': reward_mean_stddev,
    'log': reward_log,
    'no_change': reward_no_change
}
