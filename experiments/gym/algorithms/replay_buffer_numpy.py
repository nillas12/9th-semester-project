from typing import Tuple, Dict

import numpy as np


class ReplayBuffer:
    """Replay buffer containing SARS values for a number of steps. This stores values on cpu memory."""
    def __init__(self,
                 max_size: int,
                 input_dims: Tuple[int, ...]):
        self.mem_size = max_size
        self.mem_counter = 0

        self.state_memory     = np.zeros((self.mem_size, *input_dims), dtype=np.uint8)
        self.new_state_memory = np.zeros((self.mem_size, *input_dims), dtype=np.uint8)
        self.action_memory    = np.zeros(self.mem_size, dtype=np.long)
        self.reward_memory    = np.zeros(self.mem_size, dtype=np.float32)
        self.terminal_memory  = np.zeros(self.mem_size, dtype=np.bool)

    def store_transition(self, state, action, reward, new_state, done):
        """Store one set of SARS values in buffer"""
        index = self.mem_counter % self.mem_size
        self._store(index, state, action, reward, new_state, done)

    def _store(self, index, state, action, reward, new_state, done):
        """The helper function that actually stores the values. Just makes subclasses more DRY."""
        self.state_memory[index] = np.array(state)
        self.action_memory[index] = action
        self.reward_memory[index] = reward
        self.new_state_memory[index] = np.array(new_state)
        self.terminal_memory[index] = done
        self.mem_counter += 1

    def sample_buffer(self, batch_size):
        """Gets a batch of SARS values from buffer"""
        max_mem = min(self.mem_counter, self.mem_size)
        batch_indices = np.random.choice(max_mem, batch_size, replace=False)

        states     = self.state_memory[batch_indices]
        actions    = self.action_memory[batch_indices]
        rewards    = self.reward_memory[batch_indices]
        new_states = self.new_state_memory[batch_indices]
        terminal   = self.terminal_memory[batch_indices]

        return states, actions, rewards, new_states, terminal


class PrioritizedReplayBuffer(ReplayBuffer):
    """Replay buffer containing SARS values with priority for a number of steps.
    This stores values on cpu memory."""
    def __init__(self,
                 max_size: int,
                 input_dims: Tuple[int, ...],
                 alpha=0.4,
                 eps=1e-5):
        # DQfD paper uses alpha = 0.4
        super().__init__(max_size, input_dims)
        self.priorities = np.zeros(max_size, dtype=np.float32)
        self.alpha = alpha
        self.eps_a = eps

    def store_transition(self, state, action, reward, new_state, done):
        index = self.mem_counter % self.mem_size
        self._store(index, state, action, reward, new_state, done)

        # Assign max priority to new transition
        self.priorities[index] = np.max(self.priorities) if self.mem_counter > 1 else 1.0

    @property
    def max_mem(self):
        """Helper function to help subclass be more DRY"""
        return min(self.mem_counter, self.mem_size)

    def sample_buffer(self, batch_size, beta=0.6):
        # beta should be linearly annealed towards 1.0. DQfD paper starts at 0.6
        max_mem = self.max_mem
        priorities = self.priorities[:max_mem]

        probabilities = priorities ** self.alpha
        probabilities /= np.sum(probabilities)

        batch_indices = np.random.choice(max_mem, batch_size, replace=False, p=probabilities)

        weights = (max_mem * probabilities[batch_indices]) ** -beta
        weights /= np.max(weights)

        states = self.state_memory[batch_indices]
        actions = self.action_memory[batch_indices]
        rewards = self.reward_memory[batch_indices]
        new_states = self.new_state_memory[batch_indices]
        terminal = self.terminal_memory[batch_indices]

        return states, actions, rewards, new_states, terminal, batch_indices, weights

    def update_priorities(self, batch_indices, batch_priorities):
        self.priorities[batch_indices] = batch_priorities + self.eps_a


class PrioritizedReplayBufferWithDemonstrations(PrioritizedReplayBuffer):
    """Prioritized experience replay with demonstrations.
    Demonstrations are made elsewhere.
    This stores values on cpu memory.
    Consider using a Sum-tree for speedup.
    Sampling for Sum-tree is O(log n), this implementation is O(n+m*log n) time, """
    def __init__(self,
                 demonstrations: Dict[str, np.ndarray],
                 max_size: int,
                 input_dims: Tuple[int, ...],
                 alpha=0.4,
                 eps_a=0.001,
                 eps_d=1.0):
        assert len(demonstrations) == 5, "Demonstration data must be a tuple of 5 numpy arrays!"
        assert demonstrations['states'][0].shape == input_dims and \
               demonstrations['new_states'][0].shape == input_dims, "Demonstration states must have dim=input_dims!"
        (demo_states,
         demo_actions,
         demo_rewards,
         demo_new_states,
         demo_terminals) = demonstrations.values()

        self.offset = len(demo_states)

        super().__init__(max_size + self.offset, input_dims, alpha, eps_a)

        self.state_memory[:self.offset]     = demo_states
        self.action_memory[:self.offset]    = demo_actions
        self.reward_memory[:self.offset]    = demo_rewards
        self.new_state_memory[:self.offset] = demo_new_states
        self.terminal_memory[:self.offset]  = demo_terminals
        self.priorities[:self.offset]       = np.ones(self.offset, dtype=np.float32)

        self.eps_d = eps_d
        self.pre_training = True

    def store_transition(self, state, action, reward, new_state, done):
        index = (self.mem_counter % (self.mem_size - self.offset)) + self.offset
        self._store(index, state, action, reward, new_state, done)
        self.priorities[index] = np.max(self.priorities)

    def pre_training_done(self):
        self.pre_training = False

    @property
    def max_mem(self):
        return self.offset if self.pre_training else self.mem_size

    def update_priorities(self, batch_indices, batch_priorities):
        for idx, p in zip(batch_indices, batch_priorities):
            self.priorities[idx] = p + (self.eps_a if idx >= self.offset else self.eps_d)

    def is_demonstration(self, index):
        return index < self.offset

    def get_n_demo_steps(self, n, indices):
        """Returns sequences of n rewards + the next state from demonstration memory only.
           If an episode ends in one of the transitions of a sequence, then we don't include that sequence."""
        idxs = []
        mask = []
        for idx in indices:
            if idx < (self.offset - n) and not np.any(self.terminal_memory[idx:idx + n - 1]):
                idxs.append(idx)
                mask.append(True)
            else:
                mask.append(False)

        idxs = np.array(idxs)
        mask = np.array(mask)  # This is used to determine which states in a batch to get q-value from

        rewards = np.array([self.reward_memory[i:i+n] for i in idxs])
        new_states = np.array([self.new_state_memory[i+n-1] for i in idxs])
        terminals = np.array([self.terminal_memory[i+n-1] for i in idxs])

        return rewards, new_states, terminals, mask


if __name__ == "__main__":
    # Test of PrioritizedReplayBufferWithDemonstrations

    demos = 32
    demo = {
        'states': np.ones(shape=(demos, 1), dtype=np.uint8),
        'actions': np.ones(shape=demos, dtype=np.long),
        'rewards': np.ones(shape=demos, dtype=np.float32),
        'new_states': np.zeros(shape=(demos, 1), dtype=np.uint8),
        'dones': np.zeros(shape=demos, dtype=np.bool),
    }
    perd = PrioritizedReplayBufferWithDemonstrations(demo, demos, (1,))
    perd.pre_training_done()
    perd.store_transition([1], 1, 1.0, [1], False)
    x = perd.sample_buffer(32)
    for j in range(100):
        perd.store_transition([j], j, j, [j], False)
    print('x actions: ', x[1])
    y = perd.sample_buffer(32)
    print('y actions: ', y[1])
    print('possible actions: ', perd.action_memory)
    print('priorities: ', perd.priorities)
    print('n_steps: ', perd.get_n_demo_steps(10, np.array([4, 3, 2, 64])))
    print("It works!")

