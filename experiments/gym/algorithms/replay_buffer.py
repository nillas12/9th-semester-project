import torch
import numpy as np
from typing import Tuple, Dict

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


class ReplayBuffer:
    """Replay buffer containing SARS values for a number of steps"""
    def __init__(self, max_size, input_dims):
        self.mem_size = max_size
        self.mem_counter = 0
        self.cpu_counter = 0
        self.cpu_buffer_size = 50
        assert self.mem_size % self.cpu_buffer_size == 0, \
            'The gpu buffer size must be divisible by the cpu buffer size'

        self.state_memory = torch.zeros((self.mem_size, *input_dims), device=device, dtype=torch.uint8)
        self.new_state_memory = torch.zeros((self.mem_size, *input_dims), device=device, dtype=torch.uint8)
        self.action_memory = torch.zeros(self.mem_size, device=device, dtype=torch.long)
        self.reward_memory = torch.zeros(self.mem_size, device=device, dtype=torch.float32)
        self.terminal_memory = torch.zeros(self.mem_size, device=device, dtype=torch.bool)

        self.cpu_states = torch.zeros((self.cpu_buffer_size, *input_dims), device='cpu', dtype=torch.uint8)
        self.cpu_new_states = torch.zeros((self.cpu_buffer_size, *input_dims), device='cpu', dtype=torch.uint8)
        self.cpu_actions = torch.zeros(self.cpu_buffer_size, device='cpu', dtype=torch.long)
        self.cpu_rewards = torch.zeros(self.cpu_buffer_size, device='cpu', dtype=torch.float32)
        self.cpu_terminals = torch.zeros(self.cpu_buffer_size, device='cpu', dtype=torch.bool)

    def store_transition(self, state, action, reward, new_state, done):
        """Store one set of SARS values in buffer on cpu"""
        index = self.cpu_counter % self.cpu_buffer_size
        self._store(index, state, action, reward, new_state, done)

    def _store(self, index, state, action, reward, new_state, done):
        self.cpu_states[index] = torch.tensor(state)
        self.cpu_actions[index] = action
        self.cpu_rewards[index] = reward
        self.cpu_new_states[index] = torch.tensor(new_state)
        self.cpu_terminals[index] = done
        self.cpu_counter += 1
        if index == self.cpu_buffer_size - 1:
            self._store_on_gpu()

    def _store_on_gpu(self):
        """Stores a set of transitions from the cpu on the gpu.
           This is just to decrease the amount of cpu to gpu writes."""
        index = self.mem_counter % self.mem_size
        m_slice = torch.arange(index, index + self.cpu_buffer_size)
        self.state_memory[m_slice] = self.cpu_states.to(device)
        self.new_state_memory[m_slice] = self.cpu_new_states.to(device)
        self.action_memory[m_slice] = self.cpu_actions.to(device)
        self.reward_memory[m_slice] = self.cpu_rewards.to(device)
        self.terminal_memory[m_slice] = self.cpu_terminals.to(device)
        self.mem_counter += self.cpu_buffer_size

    def sample_buffer(self, batch_size):
        """Gets a batch of SARS values from buffer"""
        max_mem = min(self.mem_counter, self.mem_size)
        batch = np.random.choice(max_mem, batch_size, replace=False)

        states = self.state_memory[batch]
        actions = self.action_memory[batch]
        rewards = self.reward_memory[batch]
        new_states = self.new_state_memory[batch]
        terminal = self.terminal_memory[batch]

        return states, actions, rewards, new_states, terminal


class PrioritizedReplayBuffer(ReplayBuffer):
    """Replay buffer containing SARS values with priority for a number of steps"""
    def __init__(self,
                 max_size: int,
                 input_dims: Tuple[int, ...],
                 alpha=0.4,
                 eps=1e-5):
        # DQfD paper uses alpha = 0.4
        super().__init__(max_size, input_dims)
        self.priorities = torch.zeros(max_size, device=device, dtype=torch.float32)
        self.cpu_priorities = torch.zeros(self.cpu_buffer_size, device='cpu', dtype=torch.float32)
        self.alpha = alpha
        self.eps_a = eps

    def _store(self, index, state, action, reward, new_state, done):
        super()._store(index, state, action, reward, new_state, done)
        # Assign max priority to new transition
        self.cpu_priorities[index] = torch.max(self.priorities).cpu() if self.mem_counter > 1 else 1.0
        if index == self.cpu_buffer_size - 1:
            self._store_on_gpu()

    def _store_on_gpu(self):
        index = self.mem_counter % self.mem_size
        m_slice = torch.arange(index, index + self.cpu_buffer_size)
        super()._store_on_gpu()
        self.priorities[m_slice] = self.cpu_priorities.to(device)

    @property
    def max_mem(self):
        """Helper function to help subclass be more DRY"""
        return min(self.mem_counter, self.mem_size)

    def sample_buffer(self, batch_size, beta=0.6):
        # beta should be linearly annealed towards 1.0. DQfD paper starts at 0.6
        max_mem = self.max_mem
        priorities = self.priorities[:max_mem]

        probabilities = priorities ** self.alpha
        probabilities /= torch.sum(probabilities)

        batch_indices = np.random.choice(max_mem, batch_size, replace=False, p=probabilities.cpu().numpy())

        weights = (max_mem * probabilities[batch_indices]) ** -beta
        weights /= torch.max(weights)

        states = self.state_memory[batch_indices]
        actions = self.action_memory[batch_indices]
        rewards = self.reward_memory[batch_indices]
        new_states = self.new_state_memory[batch_indices]
        terminal = self.terminal_memory[batch_indices]

        return states, actions, rewards, new_states, terminal, batch_indices, weights

    def update_priorities(self, batch_indices, batch_priorities):
        self.priorities[batch_indices] = batch_priorities + self.eps_a


class PrioritizedReplayBufferWithDemonstrations(PrioritizedReplayBuffer):
    """Prioritized experience replay with demonstrations. Demonstrations are made elsewhere."""
    def __init__(self,
                 demonstrations: Dict[str, np.ndarray],
                 max_size: int,
                 input_dims: Tuple[int, ...],
                 alpha=0.4,
                 eps_a=0.001,
                 eps_d=1.0):
        assert len(demonstrations) == 5, "Demonstration data must be a tuple of 5 numpy arrays!"
        assert demonstrations['states'][0].shape == input_dims and \
               demonstrations['new_states'][0].shape == input_dims, "Demonstration states must have dim=input_dims!"
        super().__init__(max_size, input_dims, alpha, eps_a)

        (demo_states,
         demo_actions,
         demo_rewards,
         demo_new_states,
         demo_terminals) = demonstrations.values()

        demo_states = torch.tensor(demo_states, device=device, dtype=torch.uint8)
        demo_new_states = torch.tensor(demo_new_states, device=device, dtype=torch.uint8)
        demo_actions = torch.tensor(demo_actions, device=device, dtype=torch.long)
        demo_rewards = torch.tensor(demo_rewards, device=device, dtype=torch.float32)
        demo_terminals = torch.tensor(demo_terminals, device=device, dtype=torch.bool)

        self.offset = len(demo_states)
        self.mem_size += self.offset
        self.state_memory     = torch.cat((demo_states, self.state_memory))
        self.action_memory    = torch.cat((demo_actions, self.action_memory))
        self.reward_memory    = torch.cat((demo_rewards, self.reward_memory))
        self.new_state_memory = torch.cat((demo_new_states, self.new_state_memory))
        self.terminal_memory  = torch.cat((demo_terminals, self.terminal_memory))
        self.priorities       = torch.cat((torch.ones(self.offset, device=device, dtype=torch.float32), self.priorities))

        self.eps_d = eps_d
        self.pre_training = True

    def _store_on_gpu(self):
        index = (self.mem_counter % (self.mem_size - self.offset)) + self.offset
        m_slice = torch.arange(index, index + self.cpu_buffer_size)
        self.state_memory[m_slice] = self.cpu_states.to(device)
        self.new_state_memory[m_slice] = self.cpu_new_states.to(device)
        self.action_memory[m_slice] = self.cpu_actions.to(device)
        self.reward_memory[m_slice] = self.cpu_rewards.to(device)
        self.terminal_memory[m_slice] = self.cpu_terminals.to(device)
        self.priorities[m_slice] = self.cpu_priorities.to(device)
        self.mem_counter += self.cpu_buffer_size

    def pre_training_done(self):
        self.pre_training = False

    @property
    def max_mem(self):
        return self.offset if self.pre_training else self.mem_size

    def update_priorities(self, batch_indices, batch_priorities):
        for idx, p in zip(batch_indices, batch_priorities):
            self.priorities[idx] = p + (self.eps_a if idx >= self.offset else self.eps_d)

    def is_demonstration(self, index):
        return index < self.offset

    def get_n_demo_steps(self, n, indices):
        """Returns sequences of n rewards + the next state from demonstration memory only.
           If an episode ends in one of the transitions of a sequence, then we don't include that sequence."""
        idxs = []
        mask = []
        mask_idx = 0
        for idx in indices:
            if idx < (self.offset - n) and not torch.any(self.terminal_memory[idx:idx+n-1]):
                idxs.append(idx)
                mask.append(mask_idx)
            mask_idx += 1

        if len(idxs) == 0:
            return -1, -1, -1, -1

        idxs = np.array(idxs)
        mask = np.array(mask)  # This is used to determine which states in a batch to get q-value from

        rewards = torch.cat(tuple(self.reward_memory[i:i+n].unsqueeze(0) for i in idxs))
        new_states = torch.cat(tuple(self.new_state_memory[i+n-1].unsqueeze(0) for i in idxs))
        terminals = torch.cat(tuple(self.terminal_memory[i+n-1].unsqueeze(0) for i in idxs))

        return rewards, new_states, terminals, mask



if __name__ == "__main__":
    # Test of PrioritizedReplayBufferWithDemonstrations

    demos = 100
    demo = {
        'states': np.ones(shape=(demos, 1), dtype=np.uint8),
        'actions': np.ones(shape=demos, dtype=np.long),
        'rewards': np.ones(shape=demos, dtype=np.float32),
        'new_states': np.zeros(shape=(demos, 1), dtype=np.uint8),
        'dones': np.zeros(shape=demos, dtype=np.bool),
    }
    perd = PrioritizedReplayBufferWithDemonstrations(demo, demos, (1,))
    perd.pre_training_done()
    perd.store_transition([1], 1, 1.0, [1], False)
    x = perd.sample_buffer(32)
    for j in range(100):
        perd.store_transition([j], j, j, [j], False)
    print('x actions: ', x[1])
    y = perd.sample_buffer(32)
    print('y actions: ', y[1])
    print('possible actions: ', perd.action_memory)
    print('priorities: ', perd.priorities)
    print('n_steps: ', perd.get_n_demo_steps(10, np.array([4, 3, 2, 64])))
    print("It works!")

