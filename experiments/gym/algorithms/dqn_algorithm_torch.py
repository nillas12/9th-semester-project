import random
import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
from collections import namedtuple
from algorithm import SarsAlgorithm, Algorithm
from algorithms.replay_buffer_numpy import ReplayBuffer
from config import DqnConfig
from .reward_functions import reward_functions
from .torch_loss_functions import loss_functions

conv2d_params = namedtuple('conv2d_params',
                           ('layer_1', 'layer_2', 'layer_3'))

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


class DqnNetwork(nn.Module):
    def __init__(self, input_shape, n_actions, config: DqnConfig):
        super(DqnNetwork, self).__init__()
        in_channels = input_shape[0]
        img_width = input_shape[1]  # Image_width = image height
        out_channels = conv2d_params(*config.out_channels)
        kernel_sizes = conv2d_params(*config.kernel_sizes)
        strides = conv2d_params(*config.strides)
        fc_size = config.fully_connected_size

        def conv2d_size_out(size, kernel_size, stride):
            """Calculates the width of output convolution (7 for regular atari parameter values)"""
            return (size - (kernel_size - 1) - 1) // stride + 1

        conv_size = conv2d_size_out(
            conv2d_size_out(
                conv2d_size_out(img_width, kernel_sizes.layer_1, strides.layer_1),
                kernel_sizes.layer_2, strides.layer_2
            ),
            kernel_sizes.layer_3, strides.layer_3
        )

        self.conv1 = nn.Conv2d(in_channels,
                               out_channels.layer_1,
                               kernel_size=kernel_sizes.layer_1,
                               stride=strides.layer_1)
        self.conv2 = nn.Conv2d(out_channels.layer_1,
                               out_channels.layer_2,
                               kernel_size=kernel_sizes.layer_2,
                               stride=strides.layer_2)
        self.conv3 = nn.Conv2d(out_channels.layer_2,
                               out_channels.layer_3,
                               kernel_size=kernel_sizes.layer_3,
                               stride=strides.layer_3)

        self.fc4 = nn.Linear(conv_size * conv_size * out_channels.layer_3, fc_size)

        self.out = nn.Linear(fc_size, n_actions)

    def forward(self, x):
        x = x.float() / 255
        x = F.relu(self.conv1(x))
        x = F.relu(self.conv2(x))
        x = F.relu(self.conv3(x))
        batch_size = x.size()[0]
        flattened = x.view(batch_size, -1)
        x = F.relu(self.fc4(flattened))
        return self.out(x)


class DqnTorchAlgorithm(SarsAlgorithm):
    epoch_interval = 0.5

    def __init__(self,
                 action_space,
                 input_dims,
                 config: DqnConfig,
                 *args):
        super().__init__(action_space)
        self.action_space = [i for i in range(self.action_space.n)]
        self.gamma = config.gamma
        self.epsilon_start = config.epsilon_start
        self.epsilon_dec = config.epsilon_decay
        self.epsilon_min = config.epsilon_end
        self.batch_size = config.batch_size
        self.memory = 0
        if 'do_not_make_replay_buffer' not in args:  # A fix so we don't allocate memory twice
            self.memory = ReplayBuffer(config.memory_size, input_dims)
        self.policy_net = self._make_q_network(input_dims, action_space.n, config)
        self.target_net = self._make_q_network(input_dims, action_space.n, config)
        self.target_net.load_state_dict(self.policy_net.state_dict())
        self.target_net.eval()  # Do not update gradients of target network

        self.optimizer = optim.RMSprop(self.policy_net.parameters(),
                                       lr=config.learning_rate,
                                       # momentum=config.rms_prop_momentum,  # Gradient momentum
                                       # eps=config.rms_prop_eps  # Minimum squared gradient
                                       )

        self.iterations = 0
        self.gradient_updates = 0
        self.gradient_update_interval = config.gradient_update_interval
        self.warm_start_size = config.warm_start_size
        self.target_update_interval = config.target_update_interval
        self.epsilon_eval = config.epsilon_eval
        self.reward_function = reward_functions[config.reward_function]
        self.loss_function = loss_functions[config.loss_function]
        self.updates_per_epoch = config.updates_per_epoch

    def _make_replay_buffer(self):
        return

    def _make_optimizer(self):
        return

    @staticmethod
    def _make_q_network(input_dims, output_dims, config):
        return DqnNetwork(input_dims, output_dims, config).to(device)

    def get_action(self, observation, reward, train):
        state = torch.tensor([observation], device=device)
        action = self._choose_action(state, train)

        if train:
            self.iterations += 1
            if self.iterations % self.gradient_update_interval == 0:
                self._optimize_model()
                self.gradient_updates += 1
                self.update_target_network()

        return action

    def _choose_action(self, observation, train=True):
        """Chooses an action based on observation"""
        eps_threshold = self.epsilon_eval if not train else \
            max(self.epsilon_min, self.epsilon_start - ((self.iterations + 1) / self.epsilon_dec))

        if random.random() > eps_threshold:
            with torch.no_grad():
                return torch.argmax(self.policy_net(observation)).item()
        else:
            return random.choice(self.action_space)

    def _optimize_model(self):
        if self.memory.mem_counter < self.batch_size:
            return
        self.updates_in_epoch_step += 1
        sample = self.memory.sample_buffer(self.batch_size)
        loss = self.calculate_loss(sample)
        self.backward(loss)

    def backward(self, loss):
        self.optimizer.zero_grad()
        loss.backward()
        self.optimizer.step()

    def calculate_loss(self, sample):
        states, actions, rewards, new_states, dones = sample
        states = torch.tensor(states, device=device)
        actions = torch.tensor(actions, dtype=torch.long, device=device)
        rewards = torch.tensor(rewards, device=device)
        new_states = torch.tensor(new_states, device=device)
        dones = torch.tensor(dones, device=device)
        indices = torch.arange(self.batch_size)

        q_pred = self.policy_net(states)[indices, actions]
        q_next = self.target_net(new_states).max(dim=1)[0]

        q_next[dones] = 0.0
        q_target = rewards + self.gamma * q_next

        return self.loss_function(q_pred, q_target).to(device)

    def update_target_network(self):
        if self.gradient_updates % self.target_update_interval == 0:
            self.target_net.load_state_dict(self.policy_net.state_dict())

    def store_transition(self, state, action, reward, new_state, done):
        self.memory.store_transition(state, action, self.reward_function(reward), new_state, done)

    def save(self, path):
        torch.save({
            'policy_state': self.policy_net.state_dict(),
            'optimizer_state': self.optimizer.state_dict(),
            'iteration': self.iterations
        }, path)

    def load(self, path) -> Algorithm:
        data = torch.load(path)
        self.policy_net.load_state_dict(data['policy_state'])
        self.update_target_network()
        self.optimizer.load_state_dict(data['optimizer_state'])
        self.iterations = data['iteration']
        return self
