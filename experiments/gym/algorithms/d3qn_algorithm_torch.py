import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from collections import namedtuple
from .dqn_algorithm_torch import DqnTorchAlgorithm
from algorithms.replay_buffer import device
from config import D3qnConfig
from .replay_buffer_numpy import PrioritizedReplayBuffer

conv2d_params = namedtuple('conv2d_params',
                           ('layer_1', 'layer_2', 'layer_3'))


class DuelingDeepQNetwork(nn.Module):
    def __init__(self,
                 input_shape,
                 n_actions,
                 config: D3qnConfig):
        super(DuelingDeepQNetwork, self).__init__()
        in_channels = input_shape[0]
        img_width = input_shape[1]  # Image_width = image height
        out_channels = conv2d_params(*config.out_channels)
        kernel_sizes = conv2d_params(*config.kernel_sizes)
        strides = conv2d_params(*config.strides)
        fc_size_1 = config.fully_connected_size_1
        fc_size_2 = config.fully_connected_size_2
        self.n_actions = n_actions
        def conv2d_size_out(size, kernel_size, stride):
            """Calculates the width of output convolution (7 for regular atari parameter values)"""
            return (size - (kernel_size - 1) - 1) // stride + 1

        conv_size = conv2d_size_out(
            conv2d_size_out(
                conv2d_size_out(img_width, kernel_sizes.layer_1, strides.layer_1),
                kernel_sizes.layer_2, strides.layer_2
            ),
            kernel_sizes.layer_3, strides.layer_3
        )

        self.conv1 = nn.Conv2d(in_channels,
                               out_channels.layer_1,
                               kernel_size=kernel_sizes.layer_1,
                               stride=strides.layer_1)
        self.conv2 = nn.Conv2d(out_channels.layer_1,
                               out_channels.layer_2,
                               kernel_size=kernel_sizes.layer_2,
                               stride=strides.layer_2)
        self.conv3 = nn.Conv2d(out_channels.layer_2,
                               out_channels.layer_3,
                               kernel_size=kernel_sizes.layer_3,
                               stride=strides.layer_3)

        self.fc1_adv = nn.Linear(in_features=conv_size * conv_size * out_channels.layer_3, out_features=fc_size_2)
        self.fc1_val = nn.Linear(in_features=conv_size * conv_size * out_channels.layer_3, out_features=fc_size_2)

        self.fc2_adv = nn.Linear(in_features=fc_size_2, out_features=n_actions)
        self.fc2_val = nn.Linear(in_features=fc_size_2, out_features=1)

    def forward(self, x):
        x = x.float() / 255
        batch_size = x.size(0)
        x = F.relu(self.conv1(x))
        x = F.relu(self.conv2(x))
        x = F.relu(self.conv3(x))
        x = x.view(batch_size, -1)
        adv = F.relu(self.fc1_adv(x))
        val = F.relu(self.fc1_val(x))
        adv = self.fc2_adv(adv)
        val = self.fc2_val(val).expand(batch_size, self.n_actions)
        return val + adv - adv.mean(1).unsqueeze(1).expand(x.size(0), self.n_actions)


class DuelingDdqnTorchAlgorithm(DqnTorchAlgorithm):
    epoch_interval = 0.5

    def __init__(self,
                 action_space,
                 input_dims,
                 config: D3qnConfig,
                 *args):
        super().__init__(action_space, input_dims, config, 'do_not_make_replay_buffer')
        self.gradient_clip_norm = config.gradient_clip_norm
        self.memory = PrioritizedReplayBuffer(config.memory_size, input_dims)
        self.beta = 0.6
        self.beta_inc = 200000

    @staticmethod
    def _make_q_network(input_dims, output_dims, config):
        return DuelingDeepQNetwork(input_dims, output_dims, config).to(device)

    def _optimize_model(self):
        if self.memory.mem_counter < self.batch_size:
            return
        self.updates_in_epoch_step += 1
        beta = min(1.0, self.beta + self.gradient_updates * (1.0 - self.beta) / self.beta_inc)
        sample = self.memory.sample_buffer(self.batch_size, beta)
        loss = self.calculate_loss(sample)
        self.backward(loss)

    def backward(self, loss):
        self.optimizer.zero_grad()
        loss.backward()
        nn.utils.clip_grad_norm_(self.policy_net.parameters(), self.gradient_clip_norm)
        self.optimizer.step()

    def calculate_loss(self, sample):
        states, actions, rewards, new_states, dones, mem_indices, weights = sample
        states = torch.tensor(states, device=device)
        actions = torch.tensor(actions, dtype=torch.long, device=device)
        rewards = torch.tensor(rewards, device=device)
        new_states = torch.tensor(new_states, device=device)
        dones = torch.tensor(dones, device=device)
        indices = torch.arange(self.batch_size)

        q_pred = self.policy_net(states)[indices, actions]
        q_next = self.target_net(new_states)

        q_eval = self.policy_net(new_states)
        max_actions = torch.argmax(q_eval, dim=1)

        q_next[dones] = 0.0
        q_target = rewards + self.gamma * q_next[indices, max_actions]

        loss = (self.loss_function(q_pred, q_target, reduction='none') * torch.tensor(weights, device=device)).mean()

        td_error = np.abs(q_pred.cpu().data.numpy() - q_target.cpu().data.numpy())

        self.memory.update_priorities(mem_indices, td_error)

        return loss
