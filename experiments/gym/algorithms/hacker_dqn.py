from algorithm import SarsAlgorithm
from hackerdqn.dqn_agent import DQNAgent


class HackerDqn(SarsAlgorithm):
    epoch_interval = 1.0

    def __init__(self, action_space, input_dims):
        super().__init__(action_space)
        self.agent = DQNAgent(gamma=0.99, epsilon=1, lr=0.0001,
                         input_dims=input_dims,
                         n_actions=action_space.n, mem_size=100000, eps_min=0.1,
                         batch_size=32, replace=1000, eps_dec=1e-5)

    def get_action(self, observation, reward, train):
        old_eps = self.agent.epsilon
        if not train:
            self.agent.epsilon = 0.05
        action = self.agent.choose_action(observation)
        if train:
            self.agent.learn()
        else:
            self.agent.epsilon = old_eps
        return action

    def store_transition(self, state, action, reward, new_state, done):
        self.agent.store_transition(state, action, reward, new_state, done)

    def save(self, path):
        self.agent.q_eval.checkpoint_file = path + "_eval"
        self.agent.q_next.checkpoint_file = path + "_next"
        self.agent.save_models()

    def load(self, path):
        self.agent.q_eval.checkpoint_file = path + "_eval"
        self.agent.q_next.checkpoint_file = path + "_next"
        self.agent.load_models()

    def check_reset_epoch(self):
        if self.agent.updates_in_epoch_step >= 50_000:
            self.agent.updates_in_epoch_step = 0
            return True
        return False
