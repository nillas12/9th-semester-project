from algorithm import Algorithm


class RandomAlgorithm(Algorithm):
    def __init__(self, action_space):
        super().__init__(action_space)
        self.updates_in_epoch_step = 0

    def check_reset_epoch(self):
        if self.updates_in_epoch_step >= 10_000:
            self.updates_in_epoch_step = 0
            return True
        return False


    def episode_ended(self, observation, reward):
        pass

    def save(self, path):
        print("Saving to path {}...".format(path))

    def load(self, path) -> Algorithm:
        print("Loading from path {}".format(path))
        return self

    def step(self, observation, reward, train=True):
        return self.action_space.sample()
