#
# Reference code @: https://github.com/higgsfield/RL-Adventure
#
import tensorflow as tf
import tensorflow.keras as keras
from tensorflow.keras.optimizers import RMSprop
from tensorflow.keras.models import load_model
import numpy as np
from random import randint
from algorithms.replay_buffer import ReplayBuffer
from algorithm import SarsAlgorithm


class DuelingDeepQNetwork(keras.Model):
    def __init__(self, n_actions, fc1_dims, fc2_dims):
        super(DuelingDeepQNetwork, self).__init__()
        self.conv_1 = keras.layers.Conv2D(filters=32, kernel_size=8, strides=4, activation='relu', name="conv_1",
                                          padding="same")
        self.conv_2 = keras.layers.Conv2D(filters=64, kernel_size=4, strides=2, activation='relu', name="conv_2",
                                          padding="same")
        self.conv_3 = keras.layers.Conv2D(filters=64, kernel_size=3, strides=1, activation='relu', name="conv_3",
                                          padding="same")
        self.flatten = keras.layers.Flatten()
        self.dense1 = keras.layers.Dense(fc1_dims, activation='relu')
        self.dense2 = keras.layers.Dense(fc2_dims, activation='relu')
        self.V = keras.layers.Dense(1, activation=None)
        self.A = keras.layers.Dense(n_actions, activation=None)

    def call(self, state):
        x = tf.cast(state, tf.float32)
        x = self.conv_1(x)
        x = self.conv_2(x)
        x = self.conv_3(x)
        x = self.flatten(x)
        x = self.dense1(x)
        x = self.dense2(x)
        V = self.V(x)
        A = self.A(x)

        Q = (V + (A - tf.math.reduce_mean(A, axis=1, keepdims=True)))

        return Q

    def advantage(self, state):
        x = tf.cast(state, tf.float32)
        x = self.conv_1(x)
        x = self.conv_2(x)
        x = self.conv_3(x)
        x = self.flatten(x)
        x = self.dense1(x)
        x = self.dense2(x)
        A = self.A(x)

        return A


class DuelingDdqnAlgorithm(SarsAlgorithm):
    epoch_interval = 0.5

    def __init__(self,
                 n_actions,
                 input_dims,
                 learning_rate=0.00025,
                 gamma=0.99,
                 batch_size=32,
                 epsilon=1.0,
                 epsilon_dec=1e-3,
                 epsilon_end=0.1,
                 mem_size=100000,
                 replay_start_size=2000,
                 fc1_dims=128,
                 fc2_dims=128,
                 replace=100):
        super().__init__(n_actions)
        self.action_space = [i for i in range(n_actions.n)]
        self.gamma = gamma
        self.epsilon = epsilon
        self.epsilon_dec = epsilon_dec
        self.epsilon_min = epsilon_end
        self.replace = replace
        self.batch_size = batch_size

        self.learn_step_counter = 0
        self.replay_start_size = replay_start_size
        self.iterations = 0
        self.memory = ReplayBuffer(mem_size, input_dims)
        self.q_eval = DuelingDeepQNetwork(n_actions.n, fc1_dims, fc2_dims)
        self.q_next = DuelingDeepQNetwork(n_actions.n, fc1_dims, fc2_dims)

        self.q_eval.compile(optimizer=RMSprop(learning_rate=learning_rate, momentum=0.95),
                            loss='mean_squared_error')
        self.q_next.compile(optimizer=RMSprop(learning_rate=learning_rate, momentum=0.95),
                            loss='mean_squared_error')
        self.updates_in_epoch_step = 0

    def save(self, path):
        self.q_eval.save(path + "_eval")  # <- ugly i know
        self.q_next.save(path + "_next")

    def load(self, path):
        self.q_eval = load_model((path + "_eval"))
        self.q_next = load_model(path + "_next")

    def store_transition(self, state, action, reward, new_state, done):
        self.memory.store_transition(state, action, reward, new_state, done)

    def get_action(self, observation, reward, train=True):
        action = self.choose_action(observation, train)
        if train:
            self.learn()
        return action

    def choose_action(self, observation, train=True):
        eps = self.epsilon if train else 0.05

        if self.iterations < self.replay_start_size or np.random.random() < eps:
            action = np.random.choice(self.action_space)
        else:
            state = np.array([observation])
            actions = self.q_eval.advantage(state)
            action = tf.math.argmax(actions, axis=1).numpy()[0]

        self.iterations += 1
        return action

    def learn(self):
        if self.memory.mem_counter < self.batch_size:
            return

        self.updates_in_epoch_step += 1

        if self.learn_step_counter % self.replace == 0:
            self.q_next.set_weights(self.q_eval.get_weights())

        states, actions, rewards, new_states, dones = self.memory.sample_buffer(self.batch_size)

        q_pred = self.q_eval(states)
        q_next = self.q_next(new_states)
        q_target = q_pred.numpy()
        max_actions = tf.math.argmax(self.q_eval(new_states), axis=1)

        for idx, terminal in enumerate(dones):
            q_target[idx, actions[idx]] = rewards[idx] + \
                        self.gamma * q_next[idx, max_actions[idx]] * dones[idx]

        self.q_eval.train_on_batch(states, q_target)

        self.epsilon = self.epsilon - self.epsilon_dec \
            if self.epsilon > self.epsilon_min \
            else self.epsilon_min

        self.learn_step_counter += 1

    def check_reset_epoch(self):
        if self.updates_in_epoch_step >= 25_000:
            self.updates_in_epoch_step = 0
            return True
        return False
