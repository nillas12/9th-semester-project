import torch.nn.functional as F


loss_functions = {
    'mse': F.mse_loss,
    'smooth_l1': F.smooth_l1_loss,
    'huber': F.smooth_l1_loss
}
