from .replay_buffer import ReplayBuffer, \
                           PrioritizedReplayBuffer,\
                           PrioritizedReplayBufferWithDemonstrations,\
                           device
