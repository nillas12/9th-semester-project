from abc import ABC, abstractmethod


class Algorithm(ABC):
    epoch_interval = 0.2

    def __init__(self, action_space):
        self.action_space = action_space
        self.updates_in_epoch_step = 0
        self.updates_per_epoch = 50_000

    @abstractmethod
    def save(self, path):
        """Saves the trained algorithm / network"""

    @abstractmethod
    def load(self, path):
        """Return model loaded from path"""

    @abstractmethod
    def step(self, observation, reward, train=True):
        """Steps in time, input is observation and reward. Returns action"""

    @abstractmethod
    def populate(self, env):
        """Populates the replay buffer with a number of random transitions"""

    @abstractmethod
    def episode_ended(self, observation, reward):
        """Tells the algorithm that the current episode has ended"""

    # @abstractmethod
    def check_reset_epoch(self):
        """Checks if an epoch has passed, and reset the current epoch status to false, if result is true"""
        if self.updates_in_epoch_step >= int(self.updates_per_epoch * self.epoch_interval):
            self.updates_in_epoch_step = 0
            return True
        return False


class SarsAlgorithm(Algorithm):
    def __init__(self, action_space):
        super().__init__(action_space)
        self.old_state = None
        self.old_action = None
        self.warm_start_size = 0

    def step(self, observation, reward, train=True):
        if self.old_state is not None and self.old_action is not None and train:
            self.store_transition(self.old_state, self.old_action, reward, observation, False)
        self.old_state = observation
        self.old_action = self.get_action(observation, reward, train)
        return self.old_action

    def populate(self, env):
        observation = env.reset()
        for _ in range(self.warm_start_size):
            action = self.get_action(observation, 0, train=False)
            new_observation, reward, done, _ = env.step(action)
            self.store_transition(observation, action, reward, new_observation, False)
            observation = new_observation

    def episode_ended(self, observation, reward):
        self.store_transition(self.old_state, self.old_action, reward, observation, True)
        self.old_state = None
        self.old_action = None

    @abstractmethod
    def get_action(self, observation, reward, train):
        """Same as super.step but works as a wrapper so self.step can save action"""

    @abstractmethod
    def store_transition(self, state, action, reward, new_state, done):
        """Stores SARS values in replay buffer"""
