from collections import deque
from experiment import render_algorithm

from algorithms.a2c_algorithm_torch import A2cTorchAlgorithm
from algorithms.a2c_logic.a2c.multiprocessing_env import SubprocVecEnv, VecPyTorch, VecPyTorchFrameStack
from algorithms.a2c_logic.a2c.wrappers import TransposeImage, ClipRewardEnv, WarpFrame, FireResetEnv, EpisodicLifeEnv, \
    MaxAndSkipEnv, NoopResetEnv
from algorithms.a2c_logic.a2c.monitor import Monitor
from logger.logger import Logger
from logger.logger_factory import logger_from_environment
import torch
import gym
import numpy as np
import os
from make_env import make_env_tensor as our_make_env

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


def make_env(env_name, seed, rank):
    def _thunk():
        env = gym.make(env_name)
        env.seed(seed + rank)
        assert "NoFrameskip" in env_name, "Require environment with no frameskip"
        env = NoopResetEnv(env, noop_max=30)
        env = MaxAndSkipEnv(env, skip=4)

        allow_early_resets = False
        log_dir = 'a2c_logs/'
        assert log_dir is not None, "Log directory required for Monitor! (which is required for episodic return reporting)"
        try:
            os.mkdir(log_dir)
        except:
            pass
        env = Monitor(env, os.path.join(log_dir, str(rank)), allow_early_resets=allow_early_resets)

        env = EpisodicLifeEnv(env)
        if 'FIRE' in env.unwrapped.get_action_meanings():
            env = FireResetEnv(env)
        env = WarpFrame(env)
        # env = PyTorchFrame(env)
        env = ClipRewardEnv(env)
        # env = FrameStack(env, 4)
        obs_shape = env.observation_space.shape
        if len(obs_shape) == 3 and obs_shape[2] in [1, 3]:
            env = TransposeImage(env)

        return env
    return _thunk


def make_envs(env_name, seed, num_envs):
    envs = [make_env(env_name, seed, i) for i in range(num_envs)]
    envs = SubprocVecEnv(envs)
    envs = VecPyTorch(envs, device)
    envs = VecPyTorchFrameStack(envs, 4, device)
    return envs


def experiment_a2c(
        environment_name,
        algorithm: A2cTorchAlgorithm,
        train_epochs,
        model_upload,
        save_model,
        gif_upload,
        episodes_to_evaluate=20,
        seed=42,
        num_steps=5,
        num_envs=16,
        num_frames=int(10e6),
        log_freq=1000,
        logger: Logger = logger_from_environment(),
        start_epoch=0,
        start_with_eval=False
):
    assert num_steps > 0 and num_envs > 0
    if logger:
        logger.log_message("Starting experiment...")
        logger.set_total_epochs(train_epochs)
        logger.epoch = start_epoch
    torch.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)
    np.random.seed(seed)

    envs = make_envs(environment_name, seed, num_envs)

    # num_updates = num_frames // num_steps // num_envs

    observation = envs.reset()
    # start = time.time()
    episode_rewards = deque(maxlen=16)
    epochs = start_epoch
    is_eval = start_with_eval
    iterations = 0
    evaluated_episodes = 0
    evaluated_scores = []
    best_eval_score = -100000
    episode_cnt = 0

    while True:
        is_eval = is_eval or algorithm.check_reset_epoch()
        log_probs = []
        values = []
        rewards = []
        masks = []
        entropies = []

        for step in range(num_steps):
            observation = observation.to(device) / 255.
            actor, value = algorithm.step_actor_critic(observation, None, None)

            action = actor.sample()
            next_observation, reward, done, infos = envs.step(action.unsqueeze(1))

            for info in infos:
                if 'episode' in info.keys():
                    episode_rewards.append(info['episode']['r'])
                    episode_cnt += 1

            if not is_eval:
                log_prob = actor.log_prob(action)
                entropy = actor.entropy()

                mask = torch.from_numpy(1.0 - done).to(device).float()
                entropies.append(entropy)
                log_probs.append(log_prob)
                values.append(value.squeeze())
                rewards.append(reward.to(device).squeeze())
                masks.append(mask)

            observation = next_observation

        if not is_eval:
            algorithm.learn(next_observation, rewards, masks, log_probs, values, entropies)

        if episode_cnt >= 16:
            episode_cnt = 0
            average_reward = np.mean(episode_rewards)
            if start_with_eval:
                logger.log_message(average_reward)
            algorithm.episode_ended(observation, average_reward)
            if is_eval:
                evaluated_episodes += 1
                evaluated_scores.append(average_reward)
            else:
                if logger:
                    logger.log_metrics(average_reward)
            if is_eval and evaluated_episodes == episodes_to_evaluate:
                epochs = round(epochs + algorithm.epoch_interval, 2)
                eval_score = sum(evaluated_scores) / episodes_to_evaluate
                logger.log_epoch_evaluation(epochs, sum(evaluated_scores) / episodes_to_evaluate)
                if start_with_eval:
                    logger.log_message(np.array(evaluated_scores).std())
                best_eval_score = max(eval_score, best_eval_score)
                evaluated_scores = []
                evaluated_episodes = 0
                is_eval = False
                if model_upload and save_model:
                    algorithm.save(save_model)
                    model_upload(best_eval_score == eval_score)
                if gif_upload:
                    render_algorithm(algorithm, our_make_env(environment_name, device), 1, make_gif=True, render_human=False)
                    gif_upload(environment_name + "_1")
                if train_epochs <= epochs:
                    if logger:
                        logger.log_message("Finished training episodes...")
                    if isinstance(save_model, str):
                        if logger:
                            logger.log_message("Saving model...")
                        algorithm.save(save_model)
                    break
    if logger:
        logger.log_message("Done!")
    envs.close()
