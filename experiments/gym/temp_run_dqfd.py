from make_env import make_env
from logger.logger import Logger
from logger.logger_factory import logger_from_environment
from algorithms.dqfd_algorithm_torch import DqfdAlgorithm
import numpy as np
import os
from config import DqfdConfig
import argparse
import time

demo_folder = 'demonstrations'


def run_dqfd(env_name,
             train_epochs=10,
             save_model=False,
             logger: Logger = logger_from_environment(),
             episodes_to_evaluate=20,
             max_iterations_pr_episode=20_000,
             start_epoch=0):
    if logger:
        logger.log_message("Starting experiment...")
        logger.set_total_epochs(train_epochs)
        logger.epoch = start_epoch
    environment = make_env(env_name)
    demos = np.load(os.path.join(demo_folder, env_name)+'.npz')
    algorithm = DqfdAlgorithm(demos, environment.action_space, environment.observation_space.shape, DqfdConfig())
    from datetime import datetime
    logger.log_message("{}: Starting pre-training. This will take a while.".format(datetime.now().strftime("%H:%M:%S")))
    while algorithm.pre_training:
        algorithm.pre_train_1000_steps()
        logger.log_message("{}: Trained for {} steps".format(datetime.now().strftime("%H:%M:%S"), algorithm.pre_trained_steps))
    logger.log_message("Pre-training done!")
    reward = 0
    episode = 0
    evaluated_episodes = 0
    evaluated_scores = []
    epochs = start_epoch
    is_eval = False
    latest_reward = 0
    iterations_in_episode = 0
    best_eval_score = -100000
    observation = environment.reset()
    while True:
        is_eval = is_eval or algorithm.check_reset_epoch()
        action = algorithm.step(observation, latest_reward, not is_eval)
        new_observation, latest_reward, done, _ = environment.step(action)
        observation = new_observation
        reward += latest_reward
        iterations_in_episode += 1
        if iterations_in_episode > max_iterations_pr_episode:
            done = True
        if done:
            iterations_in_episode = 0
            algorithm.episode_ended(observation, reward)
            if is_eval:
                evaluated_episodes += 1
                evaluated_scores.append(reward)
            else:
                episode += 1
                if logger:
                    logger.log_metrics(reward)
            if is_eval and evaluated_episodes == episodes_to_evaluate:
                epochs = round(epochs + algorithm.epoch_interval, 2)
                eval_score = sum(evaluated_scores) / episodes_to_evaluate
                logger.log_epoch_evaluation(epochs, sum(evaluated_scores) / episodes_to_evaluate)
                best_eval_score = max(eval_score, best_eval_score)
                evaluated_scores = []
                evaluated_episodes = 0
                is_eval = False
                if train_epochs <= epochs:
                    if logger:
                        logger.log_message("Finished training episodes...")
                    if isinstance(save_model, str):
                        if logger:
                            logger.log_message("Saving model...")
                        algorithm.save(save_model)
                    break
            observation = environment.reset()
            reward = 0
    if logger:
        logger.log_message("Done!")
    environment.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Test run DQfD.')
    parser.add_argument('--env', type=str, default='BreakoutNoFrameskip-v4', help='The environment to train.')
    args = parser.parse_args()

    run_dqfd(args.env)
