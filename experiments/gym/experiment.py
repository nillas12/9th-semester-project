from algorithm import Algorithm
from time import sleep

from gif_builder import GifBuilder
from logger.logger import Logger
from logger.logger_factory import logger_from_environment
import numpy as np

def render_algorithm(algorithm, environment, render_episodes, render_human, make_gif, *args):
    if render_episodes == 0:
        environment.close()
        return
    gif_builder = GifBuilder(environment.unwrapped.spec.id + f"_{render_episodes}", *args)
    observation = environment.reset()
    reward = 0
    while True:
        action = algorithm.step(observation, reward, False)
        observation, reward, done, _ = environment.step(action)
        if make_gif:
            img = environment.render(mode="rgb_array")
            gif_builder.append(img, done)
        if render_human:
            environment.render(mode='human')
            sleep(0.016)
        if done:
            break
    render_algorithm(algorithm, environment, render_episodes - 1, render_human, make_gif, *args)


def experiment(
        algorithm: Algorithm,
        environment,
        train_epochs=10,
        render_episodes=5,
        save_model=False,
        logger: Logger = logger_from_environment(),
        episodes_to_evaluate=20,
        model_upload=None,
        gif_upload=None,
        max_iterations_pr_episode=20_000,
        start_epoch=0,
        start_with_eval=False
):
    if logger:
        logger.log_message("Starting experiment...")
        logger.set_total_epochs(train_epochs)
        logger.epoch = start_epoch
    reward = 0
    episode = 0
    evaluated_episodes = 0
    evaluated_scores = []
    epochs = start_epoch
    is_eval = start_with_eval
    latest_reward = 0
    iterations_in_episode = 0
    best_eval_score = -100000
    if not is_eval:
        algorithm.populate(environment)
    observation = environment.reset()
    while True:
        is_eval = is_eval or algorithm.check_reset_epoch()
        action = algorithm.step(observation, latest_reward, not is_eval)
        new_observation, latest_reward, done, _ = environment.step(action)
        observation = new_observation
        reward += latest_reward
        iterations_in_episode += 1
        if iterations_in_episode > max_iterations_pr_episode:
            done = True
        if done:
            if start_with_eval:
                logger.log_message(reward)
            iterations_in_episode = 0
            algorithm.episode_ended(observation, reward)
            if is_eval:
                evaluated_episodes += 1
                evaluated_scores.append(reward)
            else:
                episode += 1
                if logger:
                    logger.log_metrics(reward)
            if is_eval and evaluated_episodes == episodes_to_evaluate:
                epochs = round(epochs + algorithm.epoch_interval, 2)
                eval_score = sum(evaluated_scores) / episodes_to_evaluate
                logger.log_epoch_evaluation(epochs, sum(evaluated_scores) / episodes_to_evaluate)
                if start_with_eval:
                    logger.log_message(np.array(evaluated_scores).std())
                best_eval_score = max(eval_score, best_eval_score)
                evaluated_scores = []
                evaluated_episodes = 0
                is_eval = False
                if model_upload and save_model:
                    algorithm.save(save_model)
                    model_upload(best_eval_score == eval_score)
                if gif_upload:
                    render_algorithm(algorithm, environment, 1, make_gif=True, render_human=False)
                    gif_upload(environment.unwrapped.spec.id + "_1")
                if train_epochs <= epochs:
                    if logger:
                        logger.log_message("Finished training episodes...")
                    if isinstance(save_model, str):
                        if logger:
                            logger.log_message("Saving model...")
                        algorithm.save(save_model)
                    break
            observation = environment.reset()
            reward = 0
    if logger:
        logger.log_message("Done!")
    render_algorithm(algorithm, environment, render_episodes, make_gif=False, render_human=True)
    environment.close()
