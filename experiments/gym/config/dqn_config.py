from .config import Config


class DqnConfig(Config):
    def __init__(self, path='dqn.yaml'):
        super().__init__(path)
        self.kernel_sizes = self.configs['kernel_sizes']
        self.strides = self.configs['strides']
        self.out_channels = self.configs['out_channels']
        self.fully_connected_size = self.configs['fully_connected_size']
        self.batch_size = self.configs['batch_size']
        self.learning_rate = self.configs['learning_rate']
        self.gamma = self.configs['gamma']
        self.epsilon_start = self.configs['epsilon_start']
        self.epsilon_decay = self.configs['epsilon_decay']
        self.epsilon_end = self.configs['epsilon_end']
        self.epsilon_eval = self.configs['epsilon_eval']
        self.memory_size = self.configs['memory_size']
        self.warm_start_size = self.configs['warm_start_size']
        self.target_update_interval = self.configs['target_update_interval']
        self.gradient_update_interval = self.configs['gradient_update_interval']
        self.loss_function = self.configs['loss_function']
        self.rms_prop_momentum = self.configs['rms_prop_momentum']
        self.rms_prop_decay = self.configs['rms_prop_decay']
        self.rms_prop_eps = self.configs['rms_prop_eps']
        self.reward_function = self.configs['reward_function']
        self.updates_per_epoch = self.configs['updates_per_epoch']
