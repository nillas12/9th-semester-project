from .config import Config


class A2cConfig(Config):
    def __init__(self, path='a2c.yaml'):
        super().__init__(path)
        self.seed = self.configs['seed']
        self.actor_loss_coefficient = self.configs['actor_loss_coefficient']
        self.critic_loss_coefficient = self.configs['critic_loss_coefficient']
        self.entropy_loss_coefficient = self.configs['entropy_loss_coefficient']
        self.learning_rate = self.configs['learning_rate']
        self.rms_prop_alpha = self.configs['rms_prop_alpha']
        self.rms_prop_eps = self.configs['rms_prop_eps']
        self.max_grad_norm = self.configs['max_grad_norm']
        self.num_steps = self.configs['num_steps']
        self.num_envs = self.configs['num_envs']
        self.gamma = self.configs['gamma']
        self.num_frames = self.configs['num_frames']
        self.updates_per_epoch = self.configs['updates_per_epoch']
