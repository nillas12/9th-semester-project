from yaml import load, FullLoader


class Config:
    def __init__(self, path):
        with open(path) as file:
            self.configs = load(file, Loader=FullLoader)

    def get(self, key):
        """Gets a value from config."""
        return self.configs[key]

    def set(self, key, value):
        """Sets a value in the config in-memory."""
        self.configs[key] = value
