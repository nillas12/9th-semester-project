from .dqn_config import DqnConfig


class D3qnConfig(DqnConfig):
    def __init__(self, path='d3qn.yaml'):
        super().__init__(path)
        self.fully_connected_size_1 = self.fully_connected_size
        self.fully_connected_size_2 = self.configs['fully_connected_size_2']
        self.gradient_clip_norm = self.configs['gradient_clip_norm']

