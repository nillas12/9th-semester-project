from .d3qn_config import D3qnConfig


class DqfdConfig(D3qnConfig):
    def __init__(self, path='dqfd.yaml'):
        super().__init__(path)
        self.n_steps = self.configs['n_steps']
        self.alpha = self.configs['alpha']
        self.beta = self.configs['beta']
        self.beta_inc = self.configs['beta_inc']
        self.eps_a = self.configs['eps_a']
        self.eps_d = self.configs['eps_d']
        self.pre_training_steps = self.configs['pre_training_steps']
        self.l2_regularization_term = self.configs['l2_regularization_term']
        self.large_margin_term = self.configs['large_margin_term']
        self.lambda_1 = self.configs['lambda_1']
        self.lambda_2 = self.configs['lambda_2']
