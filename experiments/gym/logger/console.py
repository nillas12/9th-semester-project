from logger.logger import Logger


class Console(Logger):
    def log_epoch_implementation(self, epoch, avg_eval_score):
        print("Epoch {}/{}: {}".format(epoch, self.total_epochs, avg_eval_score))

    def __init__(self, interval=10):
        super().__init__()
        self.interval = interval
        self.counter = 0

    def _log_metrics_implementation(self, eta, last_score, best_score, avg_score,
                                    episode_duration):
        if not self.counter % self.interval:
            print(
                "Episode: {} | Latest score: {} | Best score: {} | Avg score: {} | Took ~{}s | ETA: {}..".format(
                    self.episode, last_score, best_score, round(avg_score, 2), int(episode_duration * self.interval), eta
                )
            )
        self.counter += 1

    def log_message(self, message):
        print(message)
