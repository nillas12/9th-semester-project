from abc import ABC, abstractmethod
from collections import deque
from time import time


class Logger(ABC):
    def __init__(self):
        self.best_score = -100000
        self.episode = 0
        self._time = time()
        self.times = deque(maxlen=10)
        self.epoch_start = time()
        self.epoch_time_seconds = -1
        self.total_epochs = 0
        self.prev_scores = deque(maxlen=0)
        self.epoch = 0

    def set_total_epochs(self, total_epochs):
        self.total_epochs = total_epochs
        self.prev_scores = deque(maxlen=50)

    @abstractmethod
    def log_message(self, message):
        """Logs a message"""

    def log_metrics(self, score):
        if score > self.best_score:
            self.best_score = score
        episode_time = time() - self._time
        self.prev_scores.append(score)
        self.times.append(episode_time)
        eta = -1 if self.epoch_time_seconds == -1 else int(self.epoch_time_seconds * (self.total_epochs - self.epoch))
        avg_score = sum(self.prev_scores) / len(self.prev_scores)
        self._log_metrics_implementation(
            eta,
            score,
            self.best_score,
            avg_score,
            episode_time
        )
        self.episode += 1
        self._time = time()

    def log_epoch_evaluation(self, epoch, avg_eval_score):
        self.epoch = epoch
        self.epoch_time_seconds = time() - self.epoch_start
        self.epoch_start = time()
        self.log_epoch_implementation(epoch, avg_eval_score)

    @abstractmethod
    def log_epoch_implementation(self, epoch, avg_eval_score):
        """Logs avg evaluation score at epoch"""

    @abstractmethod
    def _log_metrics_implementation(
            self,
            eta, last_score,
            best_score,
            avg_score,
            episode_duration
    ):
        """Logs relevant metrics"""
