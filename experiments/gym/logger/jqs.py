import json

import requests

from logger.logger import Logger


class Jqs(Logger):
    def __init__(self, job_id):
        super().__init__()
        self.base_url = "https://jqsapi.azurewebsites.net/"
        self.job_id = job_id

    def _log_metrics_implementation(self, eta, last_score, best_score, avg_score,
                                    episode_duration):
        payload = {
            "progress": (self.epoch / self.total_epochs) * 100,
            "eta": eta,
            "episodeDuration": episode_duration,
            "lastScore": last_score
        }
        requests.put(self.base_url + "job/" + self.job_id, data=json.dumps(payload),
                     headers={'content-type': 'application/json'})

    def log_epoch_implementation(self, epoch, avg_eval_score):
        payload = {
            "epoch": epoch,
            "score": avg_eval_score,
        }
        requests.put(self.base_url + "job/" + self.job_id + "/epoch", data=json.dumps(payload),
                     headers={'content-type': 'application/json'})

    def log_message(self, message):
        payload = {
            "message": message,
        }
        requests.put(self.base_url + "job/" + self.job_id + "/log", data=json.dumps(payload),
                     headers={'content-type': 'application/json'})
