import os

from logger.console import Console
from logger.jqs import Jqs


def logger_from_environment(job_id=None):
    if os.environ.get('IN_DOCKER', False):
        return Jqs(job_id)
    else:
        return Console(10)
