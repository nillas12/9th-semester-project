import os
import zipfile

import requests
from azure.storage.blob import BlobServiceClient

import sys
import shutil
from pathlib import Path

from experiment import experiment
from experiment_a2c import experiment_a2c
from logger.logger_factory import logger_from_environment
import traceback
from config import DqnConfig, D3qnConfig, DqfdConfig, A2cConfig
from make_env import make_env
import numpy as np


def algorithm_from_name(name, action_space, input_dims, env_name=None):
    config_base_path = "/gym/" if os.environ.get('IN_DOCKER', False) else ""
    demo_dir = 'demonstrations/'
    name = name.lower()
    if name == 'dqn':
        from algorithms.dqn_algorithm import DqnAlgorithm
        return DqnAlgorithm(action_space,
                            input_dims,
                            DqnConfig(config_base_path + "dqn.yaml"))
    elif name == 'random':
        from algorithms.random_algorithm import RandomAlgorithm
        return RandomAlgorithm(action_space)
    elif name == 'dqn_torch':
        from algorithms.dqn_algorithm_torch import DqnTorchAlgorithm
        return DqnTorchAlgorithm(action_space,
                                 input_dims,
                                 DqnConfig(config_base_path + "dqn.yaml"))
    elif name == 'd3qn':
        from algorithms.d3qn_algorithm import DuelingDdqnAlgorithm
        return DuelingDdqnAlgorithm(action_space,
                                    input_dims)
    elif name == 'd3qn_torch':
        from algorithms.d3qn_algorithm_torch import DuelingDdqnTorchAlgorithm
        return DuelingDdqnTorchAlgorithm(action_space,
                                         input_dims,
                                         D3qnConfig(config_base_path + "d3qn.yaml"))

    elif name == 'hacker_dqn':
        from algorithms.hacker_dqn import HackerDqn
        return HackerDqn(action_space, input_dims)

    elif name == 'hacker_d3qn':
        from algorithms.hacker_d3qn import HackerD3qn
        return HackerD3qn(action_space, input_dims)

    elif name == 'dqfd':
        from algorithms.dqfd_algorithm_torch import DqfdAlgorithm
        demos = np.load(config_base_path + demo_dir + env_name + '.npz')
        demos = {key: demos[key] for key in demos}
        algorithm = DqfdAlgorithm(demos, action_space, input_dims, DqfdConfig(config_base_path + 'dqfd.yaml'))
        return algorithm

    elif name == 'a2c':
        from algorithms.a2c_algorithm_torch import A2cTorchAlgorithm
        return A2cTorchAlgorithm(action_space, input_dims, A2cConfig(config_base_path + 'a2c.yaml'))

    else:
        logger.log_message("ERROR: Algorithm " + name + " was not found!")


def upload_model_to_azure(job_id, save_best=False, postfix=""):
    shutil.make_archive("model_{}".format(job_id), "zip", "models/job_{}".format(job_id))
    blob_service_client = BlobServiceClient.from_connection_string(
        "DefaultEndpointsProtocol=https;AccountName=jqs;AccountKey=Hwu2Gdpz9VUVT7K0c+AlK+YSsyUJZ"
        "mFkr8DDMec+EPSw0UKJos0VzkNTN6OvhCymjMHrZAOvI6L8SiTGPtyjEg==;EndpointSuffix=core.windows.net"
    )
    blob_client = blob_service_client.get_blob_client(container="models", blob="model_{}.zip".format(job_id))
    with open("model_{}.zip".format(job_id), "rb") as data:
        blob_client.upload_blob(data, overwrite=True)
    if save_best:
        blob_client = blob_service_client.get_blob_client(
            container="models",
            blob="model_{}_best{}.zip".format(job_id, postfix)
        )
        with open("model_{}.zip".format(job_id), "rb") as data:
            blob_client.upload_blob(data, overwrite=True)


def upload_gif_to_azure(job_id, name):
    blob_service_client = BlobServiceClient.from_connection_string(
        "DefaultEndpointsProtocol=https;AccountName=jqs;AccountKey=Hwu2Gdpz9VUVT7K0c+AlK+YSsyUJZ"
        "mFkr8DDMec+EPSw0UKJos0VzkNTN6OvhCymjMHrZAOvI6L8SiTGPtyjEg==;EndpointSuffix=core.windows.net"
    )
    blob_client = blob_service_client.get_blob_client(container="models", blob="model_{}.gif".format(job_id))
    try:
        blob_client.delete_blob()
    except:
        pass
    with open("{}.gif".format(name), "rb") as data:
        blob_client.upload_blob(data, overwrite=True)


def load_from_checkpoint(algorithm, job_id, load_best=False, epoch_postfix=""):
    link_postfix = "_best{}".format(epoch_postfix) if load_best else ""
    link = "https://jqs.blob.core.windows.net/models/model_{}{}.zip".format(job_id, link_postfix)
    r = requests.get(link, allow_redirects=True)
    open('model.zip', 'wb').write(r.content)
    with zipfile.ZipFile("model.zip", 'r') as zip_ref:
        zip_ref.extractall("models/model_{}".format(job_id))

    return algorithm.load("models/model_{}/model".format(job_id))


# example: python main.py MsPacmanNoFrameskip-v4 dqn_torch 50
if __name__ == "__main__":
    if len(sys.argv) < 4:
        print("main.py game_name algorithm_name train_episodes")
    game_name = sys.argv[1]
    algorithm_name = sys.argv[2]
    epochs = int(sys.argv[3])
    job_id = None
    save = False
    render_episodes = 5
    model_upload = False
    gif_upload = False
    start_epoch = 0
    if os.environ.get('IN_DOCKER', False):
        job_id = sys.argv[4]
        Path("models/job_{}".format(job_id)).mkdir(parents=True, exist_ok=True)
        save = "models/job_{}/model".format(job_id)
        render_episodes = 0
        gif_upload = lambda name: upload_gif_to_azure(job_id, name)
        if len(sys.argv) >= 6:
            start_epoch = int(sys.argv[5])
            model_upload = lambda is_best: upload_model_to_azure(job_id, is_best, "_" + str(start_epoch))
        else:
            model_upload = lambda is_best: upload_model_to_azure(job_id, is_best)
    logger = logger_from_environment(job_id)
    try:
        logger.log_message("Start epoch: " + str(start_epoch) + " Args: " + ", ".join(sys.argv))
        env = make_env(game_name)
        algorithm = algorithm_from_name(algorithm_name, env.action_space, env.observation_space.shape, game_name)
        if start_epoch != 0:
            # Means the algorithms should be loaded from previous checkpoint
            algorithm = load_from_checkpoint(algorithm, job_id)
        if algorithm_name not in ['a2c']:
            experiment(algorithm,
                       env,
                       train_epochs=epochs,
                       render_episodes=render_episodes,
                       save_model=save,
                       logger=logger,
                       model_upload=model_upload,
                       gif_upload=gif_upload,
                       start_epoch=start_epoch)
        else:
            experiment_a2c(game_name,
                           algorithm,
                           train_epochs=epochs,
                           model_upload=model_upload,
                           save_model=save,
                           logger=logger,
                           gif_upload=gif_upload,
                           start_epoch=start_epoch)  # ! SET ALL PARAMETERS
        if os.environ.get('IN_DOCKER', False):
            upload_model_to_azure(job_id)
    except:
        logger.log_message("ERROR: {}".format(traceback.format_exc()))
