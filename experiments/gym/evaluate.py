import sys

from experiment import experiment
from experiment_a2c import experiment_a2c
from main import load_from_checkpoint, algorithm_from_name
from make_env import make_env

# python3 evaluate.py 112 SpaceInvadersNoFrameskip-v4 dqn_torch
if __name__ == "__main__":
    run_number = sys.argv[1]
    game_name = sys.argv[2]
    algorithm_name = sys.argv[3]
    if len(sys.argv) > 4:
        epoch_postfix = "_{}".format(sys.argv[4])
    else:
        epoch_postfix = ""
    env = make_env(game_name)
    algorithm = algorithm_from_name(algorithm_name, env.action_space, env.observation_space.shape)
    algorithm = load_from_checkpoint(algorithm, run_number, load_best=True, epoch_postfix=epoch_postfix)

    if algorithm_name not in ['a2c']:
        experiment(
            algorithm,
            env,
            train_epochs=0,
            render_episodes=0,
            episodes_to_evaluate=30,
            max_iterations_pr_episode=50_000,
            start_with_eval=True
        )
    else:
        experiment_a2c(
            game_name,
            algorithm,
            train_epochs=0,
            model_upload=None,
            save_model=None,
            gif_upload=None,
            episodes_to_evaluate=30,
            start_with_eval=True
        )
