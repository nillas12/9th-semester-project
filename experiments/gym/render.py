import flask
import requests
from flask import request
from flask_cors import CORS

from experiment import render_algorithm
from make_env import make_env
from main import algorithm_from_name
import zipfile
import argparse

app = flask.Flask(__name__)
CORS(app)

parser = argparse.ArgumentParser(description="Render an episode of a gym environment.")
parser.add_argument("--gif", action="store_true", default=False, help="Generate a gif of the rendered episode.")
parser.add_argument("--fps", type=int, default=30, help="The framerate of the gif.")
parser.add_argument("--frame_skip", type=int, default=1, help="How many frames to skip. 1 means no skips.")
args = parser.parse_args()


@app.route('/', methods=['GET'])
def index():
    return "OK"


@app.route('/render', methods=['POST'])
def render():
    json = request.get_json()
    model = json["model"]
    game_name = json["env"]
    algorithm_name = json["algorithm"]
    link = "https://jqs.blob.core.windows.net/models/model_{}.zip".format(model)
    r = requests.get(link, allow_redirects=True)
    open('model.zip', 'wb').write(r.content)
    with zipfile.ZipFile("model.zip", 'r') as zip_ref:
        zip_ref.extractall("models/model_{}".format(model))

    env = make_env(game_name)
    algorithm = algorithm_from_name(algorithm_name, env.action_space, env.observation_space.shape) \
        .load("models/model_{}/model".format(model))
    render_algorithm(algorithm, env, 1, True, args.gif, args.fps, args.frame_skip)

    return "OK"


if __name__ == '__main__':
    app.run()
