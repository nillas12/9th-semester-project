@file:JsModule("dygraphs")
@file:JsNonModule
@file:Suppress("INTERFACE_WITH_SUPERCLASS", "OVERRIDING_FINAL_MEMBER", "RETURN_TYPE_MISMATCH_ON_OVERRIDE", "CONFLICTING_OVERLOADS")
package com.teknight.jobqueue

import kotlin.js.*
import kotlin.js.Json
import org.khronos.webgl.*
import org.w3c.dom.*
import org.w3c.dom.events.*
import org.w3c.dom.parsing.*
import org.w3c.dom.svg.*
import org.w3c.dom.url.*
import org.w3c.fetch.*
import org.w3c.files.*
import org.w3c.notifications.*
import org.w3c.performance.*
import org.w3c.workers.*
import org.w3c.xhr.*

external interface `T$4` {
    var errorPlotter: Any
    var linePlotter: Any
    var fillPlotter: Any
}


@JsName("default")
external open class Dygraph {
    constructor(container: HTMLElement, data: String, options: Options = definedExternally)
    constructor(container: HTMLElement, data: Array<Array<dynamic>>, options: Options = definedExternally)
    constructor(container: HTMLElement, data: Array<Array<dynamic>>, options: Options = definedExternally)
    constructor(container: HTMLElement, data: () -> dynamic, options: Options = definedExternally)
    constructor(container: String, data: String, options: Options = definedExternally)
    constructor(container: String, data: Array<Array<dynamic>>, options: Options = definedExternally)
    constructor(container: String, data: () -> dynamic, options: Options = definedExternally)
    open fun isZoomed(axis: String /* 'x' | 'y' */ = definedExternally): Boolean
    override fun toString(): String
    open fun getOption(name: String, seriesName: String = definedExternally): Any
    open fun getOptionForAxis(name: String, axis: String /* 'x' | 'y' | 'y2' */): Any
    open fun rollPeriod(): Number
    open fun xAxisRange(): dynamic /* JsTuple<Number, Number> */
    open fun xAxisExtremes(): dynamic /* JsTuple<Number, Number> */
    open fun yAxisRange(idx: Number = definedExternally): dynamic /* JsTuple<Number, Number> */
    open fun yAxisRanges(): Array<dynamic /* JsTuple<Number, Number> */>
    open fun toDomCoords(x: Number, y: Number, axis: Number = definedExternally): dynamic /* JsTuple<Number, Number> */
    open fun toDomXCoord(x: Number): Number
    open fun toDomYCoord(y: Number, axis: Number = definedExternally): Number
    open fun toDataCoords(x: Number, y: Number, axis: Number = definedExternally): dynamic /* JsTuple<Number, Number> */
    open fun toDataXCoord(x: Number): Number
    open fun toDataYCoord(y: Number, axis: Number = definedExternally): Number
    open fun toPercentYCoord(y: Number, axis: Number = definedExternally): Number
    open fun toPercentXCoord(x: Number): Number
    open fun numColumns(): Number
    open fun numRows(): Number
    open fun getValue(row: Number, col: Number): Number
    open fun destroy()
    open fun getColors(): Array<String>
    open fun getPropertiesForSeries(series_name: String): dynamic
    open fun resetZoom()
    open fun getArea(): dynamic
    open fun eventToDomCoords(event: MouseEvent): dynamic /* JsTuple<Number, Number> */
    open fun setSelection(row: Number, seriesName: String = definedExternally, locked: Boolean = definedExternally)
    open fun setSelection(row: Boolean, seriesName: String = definedExternally, locked: Boolean = definedExternally)
    open fun clearSelection()
    open fun getSelection(): Number
    open fun getHighlightSeries(): String
    open fun isSeriesLocked(): Boolean
    open fun numAxes(): Number
    open fun updateOptions(input_attrs: Options, block_redraw: Boolean = definedExternally)
    open fun resize(width: Number, height: Number)
    open fun adjustRoll(length: Number)
    open fun visibility(): Array<Boolean>
    open fun setVisibility(num: Number, value: Boolean)
    open fun setAnnotations(ann: Array<Annotation>, suppressDraw: Boolean = definedExternally)
    open fun annotations(): Array<Annotation>
    open fun getLabels(): Array<String>
    open fun indexFromSetName(name: String): Number
    open fun getRowForX(xVal: Number): Number?
    open fun ready(callback: (g: Dygraph) -> Any)

    companion object {
        var SECONDLY: Number
        var TWO_SECONDLY: Number
        var FIVE_SECONDLY: Number
        var TEN_SECONDLY: Number
        var THIRTY_SECONDLY: Number
        var MINUTELY: Number
        var TWO_MINUTELY: Number
        var FIVE_MINUTELY: Number
        var TEN_MINUTELY: Number
        var THIRTY_MINUTELY: Number
        var HOURLY: Number
        var TWO_HOURLY: Number
        var SIX_HOURLY: Number
        var DAILY: Number
        var TWO_DAILY: Number
        var WEEKLY: Number
        var MONTHLY: Number
        var QUARTERLY: Number
        var BIANNUAL: Number
        var ANNUAL: Number
        var DECADAL: Number
        var CENTENNIAL: Number
        var NUM_GRANULARITIES: Number
        var defaultInteractionModel: Any
        var DOTTED_LINE: Array<Number>
        var DASHED_LINE: Array<Number>
        var DOT_DASH_LINE: Array<Number>
        var Plotters: `T$4`
    }
}
