package com.teknight.jobqueue

import com.teknight.jobqueue.api.ApiClient
import com.teknight.jobqueue.redux.*
import kotlinx.browser.window
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import pl.treksoft.kvision.Application
import pl.treksoft.kvision.html.*
import pl.treksoft.kvision.panel.root
import pl.treksoft.kvision.require
import pl.treksoft.kvision.routing.routing
import pl.treksoft.kvision.startApplication
import pl.treksoft.kvision.state.bind
import pl.treksoft.kvision.utils.perc
import pl.treksoft.kvision.utils.px
import kotlin.js.RegExp

class App : Application() {
    init {
        require("css/jq.css")
        require("regression")
        require("dygraphs")
    }

    override fun start() {
        root("jqapp", addRow = false) {
            row {
                col("text-center", "pb-3") {
                    link("", url = "/#!/") {
                        image(require("img/hclLogo.png")) {
                            width = 20.perc
                            maxWidth = 200.px
                        }
                    }.onClick {
                        routeStore.dispatch(RouteActions.ClearDetailedJob)
                        ApiClient.getJobs(false)
                    }
                }
            }
            tag(TAG.DIV).bind(routeStore) { state ->
                when (state.showDetailedJob) {
                    false -> index(state)
                    true -> jobPage(state)
                }
            }
            row {
                col("text-center", "pt-3") {
                    button("Toggle detailed").onClick {
                        routeStore.dispatch(RouteActions.ToggleDetailedView)
                        val jobId = routeStore.getState().detailedJobId
                        if (jobId != null) {
                            val job = routeStore.getState().selectedJob
                            ApiClient.getJob(jobId)
                        } else {
                            ApiClient.getJobs(false)
                        }
                    }
                }
            }
        }
        routing.on({ _ -> routeStore.dispatch(RouteActions.ClearDetailedJob) })
                .on(RegExp("/job/(.*)"), { x -> routeStore.dispatch(RouteActions.GotoDetailedJob(x)) })
                .resolve()
        ApiClient.getPullerStatus()
        ApiClient.getRenderServer()
        GlobalScope.launch {
            window.setInterval({
                if (!routeStore.getState().detailedView) {
                    val jobId = routeStore.getState().detailedJobId
                    if (jobId != null) {
                        val job = routeStore.getState().selectedJob
                        if (job?.score?.count() ?: 2600 < 2500 && job?.job?.jobStatus != JobStatus.Completed) {
                            ApiClient.getJob(jobId)
                        }
                    } else {
                        ApiClient.getJobs(false)
                    }
                }
            }, 15000)
        }
    }
}

fun main() {
    startApplication(::App)
}
