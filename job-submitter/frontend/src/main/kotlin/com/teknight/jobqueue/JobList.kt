package com.teknight.jobqueue

import com.teknight.jobqueue.api.ApiClient
import com.teknight.jobqueue.redux.JobShort
import com.teknight.jobqueue.redux.JobStatus
import pl.treksoft.kvision.core.*
import pl.treksoft.kvision.html.*
import pl.treksoft.kvision.routing.routing
import pl.treksoft.kvision.table.cell
import pl.treksoft.kvision.table.row
import pl.treksoft.kvision.table.table

fun statusBadge(status: JobStatus) = when (status) {
    JobStatus.Pending -> "badge-warning"
    JobStatus.Running -> "badge-info"
    JobStatus.Completed -> "badge-success"
    JobStatus.Failed -> "badge-danger"
    JobStatus.Initializing -> "badge-info"
    JobStatus.Canceling -> "badge-warning"
    JobStatus.Canceled -> "badge-danger"
}

fun Container.jobList(jobs: List<JobShort>, renderAvailable: Boolean) {
    div(classes = setOf("table-responsive-lg")) {
        val headers = listOf("Job #", "Status", "Progress", "Epochs", "ETA", "LEE", "Env", "Algorithm") + if (renderAvailable) "Render" else ""
        table(headers, classes = setOf("table")) {
            jobs.forEach {
                val jobId = it.jobId
                this.row {
                    style {
                        cursor = Cursor.POINTER
                    }
                    cell { +it.jobId }
                    cell { span(classes = setOf("badge", "badge-pill", statusBadge(it.jobStatus))) { +it.jobStatus.name } }
                    cell { +"${it.progress}%" }
                    cell { +"${it.trainingEpisodes}" }
                    cell { +it.eta }
                    cell { +it.lastEpisodeEnded }
                    cell { +it.gymEnvironment }
                    cell { +(algorithmsMap[it.algorithm] ?: it.algorithm) }
                    if (renderAvailable) {
                        if (it.jobStatus == JobStatus.Completed) {
                            cell {
                                button("", "fas fa-play", ButtonStyle.SECONDARY).onClick { _ ->
                                    ApiClient.render(it)
                                }
                            }
                        } else cell {  }
                    }
                }.onEvent {
                    click = {
                        routing.navigate("#!/job/${jobId}")
                    }
                    mouseenter = {
                        self.background = Background(Color("#F3F3F3"))
                    }
                    mouseleave = {
                        self.background = Background(Color("#FFFFFF"))
                    }
                }
            }
        }
    }
}
