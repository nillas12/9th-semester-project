package com.teknight.jobqueue

import pl.treksoft.kvision.core.Container
import pl.treksoft.kvision.html.Div
import pl.treksoft.kvision.html.div

fun Container.row(vararg classes: String, init: Div.() -> Unit) = div(classes = classes.toSet() + "row", init = init)
fun Container.col(vararg classes: String, init: Div.() -> Unit) = div(classes = classes.toSet() + "col-xl", init = init)
fun Container.colAuto(vararg classes: String, init: Div.() -> Unit) = div(classes = classes.toSet() + "col-xl-auto", init = init)
fun Container.col1(vararg classes: String, init: Div.() -> Unit) = div(classes = classes.toSet() + "col-xl-1", init = init)
fun Container.col2(vararg classes: String, init: Div.() -> Unit) = div(classes = classes.toSet() + "col-xl-2", init = init)
fun Container.col3(vararg classes: String, init: Div.() -> Unit) = div(classes = classes.toSet() + "col-xl-3", init = init)
fun Container.col4(vararg classes: String, init: Div.() -> Unit) = div(classes = classes.toSet() + "col-xl-4", init = init)
fun Container.col5(vararg classes: String, init: Div.() -> Unit) = div(classes = classes.toSet() + "col-xl-5", init = init)
fun Container.col6(vararg classes: String, init: Div.() -> Unit) = div(classes = classes.toSet() + "col-xl-6", init = init)
fun Container.col7(vararg classes: String, init: Div.() -> Unit) = div(classes = classes.toSet() + "col-xl-7", init = init)
fun Container.col8(vararg classes: String, init: Div.() -> Unit) = div(classes = classes.toSet() + "col-xl-8", init = init)
fun Container.col9(vararg classes: String, init: Div.() -> Unit) = div(classes = classes.toSet() + "col-xl-9", init = init)
fun Container.col10(vararg classes: String, init: Div.() -> Unit) = div(classes = classes.toSet() + "col-xl-10", init = init)
fun Container.col11(vararg classes: String, init: Div.() -> Unit) = div(classes = classes.toSet() + "col-xl-11", init = init)
fun Container.col12(vararg classes: String, init: Div.() -> Unit) = div(classes = classes.toSet() + "col-xl-12", init = init)
