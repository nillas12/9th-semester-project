package com.teknight.jobqueue

import com.teknight.jobqueue.api.ApiClient
import com.teknight.jobqueue.api.JobCreation
import com.teknight.jobqueue.redux.PullStatus
import com.teknight.jobqueue.redux.PullerStatus
import kotlinx.serialization.Serializable
import pl.treksoft.kvision.core.Container
import pl.treksoft.kvision.form.formPanel
import pl.treksoft.kvision.form.select.Select
import pl.treksoft.kvision.form.text.Text
import pl.treksoft.kvision.form.text.Typeahead
import pl.treksoft.kvision.html.*

@Serializable
data class Form(
        val gymEnvironment: String,
        val algorithm: String,
        val trainEpisodes: String?,
        val dockerImageVersion: String?
)

fun Container.jobForm(jobPullerStatus: List<PullStatus>) {
    val form = formPanel<Form> {
        add(
                Form::gymEnvironment,
                Typeahead(
                        listOf(
                                "AdventureNoFrameskip-v0", "AdventureNoFrameskip-v4", "AirRaidNoFrameskip-v0", "AirRaidNoFrameskip-v4", "AlienNoFrameskip-v0", "AlienNoFrameskip-v4", "AmidarNoFrameskip-v0", "AmidarNoFrameskip-v4", "AssaultNoFrameskip-v0", "AssaultNoFrameskip-v4", "AsterixNoFrameskip-v0", "AsterixNoFrameskip-v4", "AsteroidsNoFrameskip-v0", "AsteroidsNoFrameskip-v4", "AtlantisNoFrameskip-v0", "AtlantisNoFrameskip-v4", "BankHeistNoFrameskip-v0", "BankHeistNoFrameskip-v4", "BattleZoneNoFrameskip-v0", "BattleZoneNoFrameskip-v4", "BeamRiderNoFrameskip-v0", "BeamRiderNoFrameskip-v4", "BerzerkNoFrameskip-v0", "BerzerkNoFrameskip-v4", "BowlingNoFrameskip-v0", "BowlingNoFrameskip-v4", "BoxingNoFrameskip-v0", "BoxingNoFrameskip-v4", "BreakoutNoFrameskip-v0", "BreakoutNoFrameskip-v4", "CarnivalNoFrameskip-v0", "CarnivalNoFrameskip-v4", "CentipedeNoFrameskip-v0", "CentipedeNoFrameskip-v4", "ChopperCommandNoFrameskip-v0", "ChopperCommandNoFrameskip-v4", "CrazyClimberNoFrameskip-v0", "CrazyClimberNoFrameskip-v4", "DefenderNoFrameskip-v0", "DefenderNoFrameskip-v4", "DemonAttackNoFrameskip-v0", "DemonAttackNoFrameskip-v4", "DoubleDunkNoFrameskip-v0", "DoubleDunkNoFrameskip-v4", "ElevatorActionNoFrameskip-v0", "ElevatorActionNoFrameskip-v4", "EnduroNoFrameskip-v0", "EnduroNoFrameskip-v4", "FishingDerbyNoFrameskip-v0", "FishingDerbyNoFrameskip-v4", "FreewayNoFrameskip-v0", "FreewayNoFrameskip-v4", "FrostbiteNoFrameskip-v0", "FrostbiteNoFrameskip-v4", "GopherNoFrameskip-v0", "GopherNoFrameskip-v4", "GravitarNoFrameskip-v0", "GravitarNoFrameskip-v4", "HeroNoFrameskip-v0", "HeroNoFrameskip-v4", "IceHockeyNoFrameskip-v0", "IceHockeyNoFrameskip-v4", "JamesbondNoFrameskip-v0", "JamesbondNoFrameskip-v4", "JourneyEscapeNoFrameskip-v0", "JourneyEscapeNoFrameskip-v4", "KangarooNoFrameskip-v0", "KangarooNoFrameskip-v4", "KrullNoFrameskip-v0", "KrullNoFrameskip-v4", "KungFuMasterNoFrameskip-v0", "KungFuMasterNoFrameskip-v4", "MontezumaRevengeNoFrameskip-v0", "MontezumaRevengeNoFrameskip-v4", "MsPacmanNoFrameskip-v0", "MsPacmanNoFrameskip-v4", "NameThisGameNoFrameskip-v0", "NameThisGameNoFrameskip-v4", "PhoenixNoFrameskip-v0", "PhoenixNoFrameskip-v4", "PitfallNoFrameskip-v0", "PitfallNoFrameskip-v4", "PongNoFrameskip-v0", "PongNoFrameskip-v4", "PooyanNoFrameskip-v0", "PooyanNoFrameskip-v4", "PrivateEyeNoFrameskip-v0", "PrivateEyeNoFrameskip-v4", "QbertNoFrameskip-v0", "QbertNoFrameskip-v4", "RiverraidNoFrameskip-v0", "RiverraidNoFrameskip-v4", "RoadRunnerNoFrameskip-v0", "RoadRunnerNoFrameskip-v4", "RobotankNoFrameskip-v0", "RobotankNoFrameskip-v4", "SeaquestNoFrameskip-v0", "SeaquestNoFrameskip-v4", "SkiingNoFrameskip-v0", "SkiingNoFrameskip-v4", "SolarisNoFrameskip-v0", "SolarisNoFrameskip-v4", "SpaceInvadersNoFrameskip-v0", "SpaceInvadersNoFrameskip-v4", "StarGunnerNoFrameskip-v0", "StarGunnerNoFrameskip-v4", "TennisNoFrameskip-v0", "TennisNoFrameskip-v4", "TimePilotNoFrameskip-v0", "TimePilotNoFrameskip-v4", "TutankhamNoFrameskip-v0", "TutankhamNoFrameskip-v4", "UpNDownNoFrameskip-v0", "UpNDownNoFrameskip-v4", "VentureNoFrameskip-v0", "VentureNoFrameskip-v4", "VideoPinballNoFrameskip-v0", "VideoPinballNoFrameskip-v4", "WizardOfWorNoFrameskip-v0", "WizardOfWorNoFrameskip-v4", "YarsRevengeNoFrameskip-v0", "YarsRevengeNoFrameskip-v4", "ZaxxonNoFrameskip-v0", "ZaxxonNoFrameskip-v4"
                        ),
                        label = "Name of gym environmet"
                ).apply {
                    placeholder = "MsPacmanNoFrameskip-V4"
                },
                required = true,
        ) {
            it.getValue()?.isNotBlank()
        }
        add(
                Form::algorithm, Select(
                options = algorithms.filterNot { it.second.contains("Deprecated") },
                label = "Algorithm",
                value = "dqn"
        ),
                required = true
        )
        add(
                Form::trainEpisodes,
                Text(label = "Epochs to train for").apply {
                    placeholder = "50"
                },
                validatorMessage = { "Only numbers are allowed" }) {
            it.getValue()?.matches("^[0-9]+$") ?: true
        }
        add(
                Form::dockerImageVersion,
                Text(label = "Docker image version").apply {
                    placeholder = "latest"
                },
        )
        add(
                div {
                    p {
                        +"Job puller status: "
                        jobPullerStatus.forEach {
                            span(classes = setOf("badge", "mx-1", "p-2", "badge-pill", when(it.pullStatus) {
                                PullerStatus.Ready -> "badge-success"
                                PullerStatus.Running -> "badge-info"
                                PullerStatus.Down -> "badge-danger"
                            })) { +it.host }
                        }
                    }
                }
        )
    }
    form.add(
            div(classes = setOf("text-center")) {
                button("Submit", style = ButtonStyle.PRIMARY).onClick {
                    if (form.validate()) {
                        val formValue = form.getData()
                        val jobCreation = JobCreation(
                                formValue.gymEnvironment,
                                formValue.trainEpisodes?.toInt() ?: 50,
                                formValue.algorithm,
                                formValue.dockerImageVersion ?: "latest"
                        )
                        ApiClient.createJob(jobCreation)
                    }
                }
            }
    )
}