package com.teknight.jobqueue.redux

import kotlinx.serialization.Serializable

enum class JobStatus {
    Pending,
    Running,
    Completed,
    Failed,
    Initializing,
    Canceling,
    Canceled
}

@Serializable
data class JobShort(
        val jobId: String,
        val lastEpisodeEnded: String,
        val progress: Int,
        val gymEnvironment: String,
        val eta: String,
        val jobStatus: JobStatus,
        val trainingEpisodes: Int,
        val algorithm: String,
        val dockerImageVersion: String,
        val episodeDuration: String,
        val host: String
)

@Serializable
data class JobLongReceive(val job: JobShort, val score: List<Pair<Int, Double>>, val logs: List<Pair<String, String>>, val epochScore: List<Pair<Double, Double>>)

@Serializable
data class JobLong(val job: JobShort, val score: Map<Int, Double>, val logs: List<Pair<String, String>>, val epochScore: List<Pair<Double, Double>>)

@Serializable
data class State(val jobs: List<JobShort>, val loadingJobs: Boolean)

@Serializable
data class RouteState(val detailedJobId: String?, val selectedJob: JobLong?, val JobPullerStatus: List<PullStatus>, val renderAvailable: Boolean, val detailedView: Boolean) {
    val showDetailedJob get() = detailedJobId != null
}

enum class PullerStatus {
    Ready,
    Running,
    Down
}

@Serializable
data class PullStatus(val pullStatus: PullerStatus, val host: String)

val initialState = State(emptyList(), false)
val initialRouteState = RouteState(null, null, listOf(), false, false)
