package com.teknight.jobqueue

import com.teknight.jobqueue.api.ApiClient
import com.teknight.jobqueue.api.JobCreation
import com.teknight.jobqueue.api.JobResume
import com.teknight.jobqueue.redux.JobStatus
import com.teknight.jobqueue.redux.RouteState
import kotlinx.browser.window
import pl.treksoft.kvision.core.Container
import pl.treksoft.kvision.form.text.textArea
import pl.treksoft.kvision.html.*
import pl.treksoft.kvision.table.cell
import pl.treksoft.kvision.table.row
import pl.treksoft.kvision.table.table
import kotlin.math.round


fun Container.jobPage(state: RouteState) {
    pl.treksoft.kvision.require("regression")
    col("p-5", "card", "shadow") {
        if (state.selectedJob == null) {
            state.detailedJobId?.let { ApiClient.getJob(it) }
            spinner()
        } else {
            state.selectedJob.let {
                row("text-center") {
                    col {
                        h6("Job id")
                        h4("#${it.job.jobId}")
                    }
                    col {
                        h6("Environment")
                        h4(it.job.gymEnvironment)
                    }
                    col {
                        h6("Algorithm")
                        h4(algorithmsMap[it.job.algorithm] ?: it.job.algorithm)
                    }
                    col {
                        h6("Progress")
                        h4("${it.job.progress}%")
                    }
                    col {
                        h6("Status")
                        h4 { span(classes = setOf("badge", "badge-pill", statusBadge(it.job.jobStatus))) { +it.job.jobStatus.name } }
                    }
                }
                tag(TAG.HR)
                row {
                    col4 {
                        table(classes = setOf("table")) {
                            this.row {
                                cell {
                                    +"Episodes"
                                }
                                cell {
                                    +"${it.score.maxByOrNull { it.key }?.key ?: "0"}"
                                }
                            }
                            this.row {
                                cell {
                                    +"Epochs"
                                }
                                cell {
                                    +"${it.epochScore.maxByOrNull { it.first }?.first?.withSingleDigit ?: "-"}/${it.job.trainingEpisodes}"
                                }
                            }
                            this.row {
                                cell {
                                    +"Latest evaluation score"
                                }
                                cell {
                                    +"${it.epochScore.lastOrNull()?.second ?: "-"}"
                                }
                            }
                            this.row {
                                cell {
                                    +"Latest score"
                                }
                                cell {
                                    +"${it.score.maxByOrNull { it.key }?.value ?: "-"}"
                                }
                            }
                            this.row {
                                cell {
                                    +"Max score"
                                }
                                cell {
                                    +"${it.score.maxByOrNull { it.value }?.value ?: "-"}"
                                }
                            }
                            this.row {
                                cell {
                                    +"ETA"
                                }
                                cell {
                                    +it.job.eta
                                }
                            }
                            this.row {
                                cell {
                                    +"LEE"
                                }
                                cell {
                                    +it.job.lastEpisodeEnded
                                }
                            }
                            this.row {
                                cell {
                                    +"Episode duration"
                                }
                                cell {
                                    +it.job.episodeDuration
                                }
                            }
                            this.row {
                                cell {
                                    +"Image version"
                                }
                                cell {
                                    +it.job.dockerImageVersion
                                }
                            }
                            this.row {
                                cell {
                                    +"Host"
                                }
                                cell {
                                    +it.job.host
                                }
                            }
                        }
                        tag(TAG.HR)
                        row {
                            col {
                                textArea(rows = 9, value = it.logs.joinToString("\n") { "[${it.first}]: ${it.second}" })
                            }
                            colAuto("text-center") {
                                image("https://jqs.blob.core.windows.net/models/model_${it.job.jobId}.gif", alt = "")
                            }
                        }
                    }
                    col8 {
                        if (it.score.isNotEmpty()) {
                            chart(it.score)
                        }
                        div(classes = setOf("my-3"))
                        if (it.epochScore.isNotEmpty()) {
                            epochChart(it.epochScore)
                        }
                    }
                }
                row {
                    col("text-center") {
                        if (it.epochScore.isNotEmpty() && state.renderAvailable) {
                            button("Render latest", classes = setOf("mx-2")).onClick { _ ->
                                ApiClient.render(it.job)
                            }
                        }
                        when (it.job.jobStatus) {
                            JobStatus.Canceled,
                            JobStatus.Canceling,
                            JobStatus.Failed -> {
                                if (it.epochScore.isNotEmpty()) {
                                    button("Resume from checkpoint", style = ButtonStyle.SUCCESS, classes = setOf("mx-2")).onClick { _ ->
                                        ApiClient.continueJob(
                                                JobResume(
                                                        it.job.jobId,
                                                        window.prompt("Please specify total epochs", "${it.job.trainingEpisodes}")?.toIntOrNull() ?: it.job.trainingEpisodes
                                                )
                                        )
                                    }
                                }
                                button("Resubmit", style = ButtonStyle.PRIMARY).onClick { _ ->
                                    ApiClient.createJob(
                                            JobCreation(
                                                    it.job.gymEnvironment,
                                                    it.job.trainingEpisodes,
                                                    it.job.algorithm,
                                                    it.job.dockerImageVersion
                                            )
                                    )
                                }
                            }
                            JobStatus.Pending,
                            JobStatus.Initializing,
                            JobStatus.Running ->
                                button("Cancel", style = ButtonStyle.WARNING).onClick { _ ->
                                    ApiClient.cancelJob(it.job.jobId)
                                }
                            JobStatus.Completed ->
                                button("Download", style = ButtonStyle.INFO).onClick { _ ->
                                    window.location.href = "https://jqs.blob.core.windows.net/models/model_${it.job.jobId}_best.zip"
                                }
                        }

                    }
                }
            }
        }
    }
}

val Double.withSingleDigit get() = round(this * 10.0) / 10