package com.teknight.jobqueue.redux

import pl.treksoft.kvision.redux.createReduxStore

val store = createReduxStore(::reducer, initialState)
val routeStore = createReduxStore(::routeReducer, initialRouteState)
