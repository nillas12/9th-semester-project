package com.teknight.jobqueue.redux

import pl.treksoft.kvision.redux.RAction

sealed class RouteActions: RAction {
    data class GotoDetailedJob(val jobId: String) : RouteActions()
    object ClearDetailedJob : RouteActions()
    data class LoadJob(val job: JobLong) : RouteActions()
    data class SetJobPullerStatus(val pullerStatus: List<PullStatus>): RouteActions()
    object EnableRendering: RouteActions()
    object ToggleDetailedView: RouteActions()
}

sealed class Actions : RAction {
    data class LoadJobs(val jobs: List<JobShort>) : Actions()
    object StartLoadingJobs: Actions()
}