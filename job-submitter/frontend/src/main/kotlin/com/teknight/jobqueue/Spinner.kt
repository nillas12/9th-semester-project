package com.teknight.jobqueue

import pl.treksoft.kvision.core.Container
import pl.treksoft.kvision.html.div
import pl.treksoft.kvision.html.span
import pl.treksoft.kvision.utils.px

fun Container.spinner() {
    row {
        col("text-center") {
            div(classes = setOf("spinner-border", "text-secondary", "p-3")) {
                role = "status"
                width = 100.px
                height = 100.px
                span(classes = setOf("sr-only")) { +"Loading..." }
            }
        }
    }
}