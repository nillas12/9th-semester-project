package com.teknight.jobqueue.api

import com.teknight.jobqueue.redux.*
import com.teknight.jobqueue.redux.store
import kotlinx.browser.window
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.asDeferred
import kotlinx.coroutines.launch
import kotlinx.serialization.Serializable
import pl.treksoft.kvision.rest.HttpMethod
import pl.treksoft.kvision.rest.RestClient

object ApiClient {
    private val restClient = RestClient()
    // private const val baseUrl = "http://localhost:7000/"
    private const val baseUrl = "https://jqsapi.azurewebsites.net/"

    fun getJobs(clearExisting: Boolean = true) {
        if (clearExisting) {
            store.dispatch(Actions.StartLoadingJobs)
        }
        GlobalScope.launch {
            val prefix = if (routeStore.getState().detailedView) "detailed/" else ""
            val jobs = restClient.call<List<JobShort>>(baseUrl + prefix + "job").asDeferred().await()
            store.dispatch(Actions.LoadJobs(jobs))
        }
    }

    fun createJob(jobCreation: JobCreation) {
        store.dispatch(Actions.StartLoadingJobs)
        GlobalScope.launch {
            val jobs = restClient.call<List<JobShort>, JobCreation>(baseUrl + "job", jobCreation, HttpMethod.POST).asDeferred().await()
            store.dispatch(Actions.LoadJobs(jobs))
        }
    }

    fun continueJob(jobResume: JobResume) {
        store.dispatch(Actions.StartLoadingJobs)
        GlobalScope.launch {
            val jobs = restClient.call<List<JobShort>, JobResume>(baseUrl + "job/resume", jobResume, HttpMethod.POST).asDeferred().await()
            store.dispatch(Actions.LoadJobs(jobs))
        }
    }

    fun getPullerStatus() {
        GlobalScope.launch {
            val pullStatus = restClient.call<List<PullStatus>>(baseUrl + "pull").asDeferred().await()
            routeStore.dispatch(RouteActions.SetJobPullerStatus(pullStatus))
        }
    }

    fun getJob(id: String) {
        GlobalScope.launch {
            val postfix = if (routeStore.getState().detailedView) "?detailed=true" else ""
            val job = restClient.call<JobLongReceive>(baseUrl + "job/$id" + postfix).asDeferred().await()
            routeStore.dispatch(RouteActions.LoadJob(JobLong(job.job, job.score.toMap(), job.logs, job.epochScore)))
        }
    }

    fun cancelJob(id: String) {
        GlobalScope.launch {
            val jobs = restClient.call<List<JobShort>>(baseUrl + "job/$id", method = HttpMethod.DELETE).asDeferred().await()
            store.dispatch(Actions.LoadJobs(jobs))
            getJob(id)
        }
    }

    fun getRenderServer() {
        GlobalScope.launch {
            try {
                restClient.remoteCall("http://localhost:5000").asDeferred().await()
                routeStore.dispatch(RouteActions.EnableRendering)
            } catch (e: Exception) {
                console.log("Rendering not enabled")
            }
        }
    }

    @Serializable
    data class RenderRequest(val model: String, val env: String, val algorithm: String)
    fun render(job: JobShort) {
        GlobalScope.launch {
            try {
                restClient.call<String, RenderRequest>("http://localhost:5000/render", RenderRequest(
                        job.jobId,
                        job.gymEnvironment,
                        job.algorithm
                ), HttpMethod.POST)
            } catch (e: Exception) {
                window.alert("Request to render server failed: ${e.message}")
            }
        }
    }
}