package com.teknight.jobqueue

import org.w3c.dom.CanvasRenderingContext2D
import pl.treksoft.kvision.core.Container
import pl.treksoft.kvision.html.div
import pl.treksoft.kvision.utils.obj
import pl.treksoft.kvision.utils.perc
import pl.treksoft.kvision.utils.px

fun Container.chart(score: Map<Int, Double>) {
    val orderedKeys = score.keys.sorted()
    val sa = slidingAverage(score, orderedKeys)
    val lr = linearRegression(score, orderedKeys)
    val max = maxScore(score, orderedKeys)
    val data = orderedKeys.map { index ->
        arrayOf(index, score[index], sa[index], lr[index], max[index])
    }.toTypedArray()
    div { id = "chartdiv" }.apply {
        width = 100.perc
        height = 58.perc
        minHeight = 450.px
        afterInsertHook = {
            Dygraph("chartdiv", data, obj {
                // customBars = true
                showRangeSelector = true
                labels = arrayOf("Episode", "Score", "Sliding", "LR", "Max")
                connectSeparatedPoints = true
                titleHeight = 32
                series = obj {
                    Score = obj {
                        strokeWidth = 0.0
                        drawPoints = true
                        pointSize = 3
                        highlightCircleSize = 6
                        this.color = "#90ee90"
                        strokeBorderWidth = 3
                        drawPointCallback = pointDrawer
                        drawHighlightPointCallback = pointDrawer
                    }
                    LR = obj {
                        strokeWidth = 2.0
                        this.color = "#ffa500"
                    }
                    Max = obj {
                        strokeWidth = 2.0
                        this.color = "#168b16"
                        stepPlot = true
                    }
                    Sliding = obj {
                        strokeWidth = 2.0
                        this.color = "#9370db"
                    }
                }
            })
        }
    }
}

val pointDrawer = { g: dynamic, seriesName: String, canvasContext: CanvasRenderingContext2D, cx: Double, cy: Double, color: String, pointSize: Double ->
    canvasContext.fillStyle = color
    canvasContext.beginPath()
    canvasContext.arc(cx, cy, pointSize, 0.0, 2.0 * 3.141593, false)
    canvasContext.fill()
    canvasContext.lineWidth = 1.0
    canvasContext.strokeStyle = "#003300"
    canvasContext.stroke()
}

fun slidingAverage(score: Map<Int, Double>, orderedKeys: List<Int>) = orderedKeys.windowed(50, 1) {
    it.last() to it.sumByDouble { i -> score.getValue(i)} / 50
}.toMap()

fun linearRegression(score: Map<Int, Double>, orderedKeys: List<Int>) = linear(orderedKeys.map { index -> arrayOf(index.toDouble(), score[index]) }.toTypedArray(), obj { precision = 4 }).points.map { it[0] as Int to it[1] }.toMap()

data class MaxFolder(val max: Double, val scores: Map<Int, Double>)

fun maxScore(score: Map<Int, Double>, orderedKeys: List<Int>) = orderedKeys.fold(MaxFolder(Double.NEGATIVE_INFINITY, emptyMap())) { acc, i ->
    if (score[i] ?: (Double.NEGATIVE_INFINITY) > acc.max) {
        acc.copy(max = score.getValue(i), scores = acc.scores + (i to score.getValue(i)))
    } else {
        acc
    }
}.let { it.scores + (orderedKeys.last() to it.max) }