package com.teknight.jobqueue.api

import kotlinx.serialization.Serializable

@Serializable
data class JobCreation(val env: String, val trainingEpisodes: Int, val algorithm: String, val dockerImage: String)

@Serializable
data class JobResume(val jobId: String, val epochs: Int)

