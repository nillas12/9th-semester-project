package com.teknight.jobqueue

import pl.treksoft.kvision.core.Container
import pl.treksoft.kvision.html.div
import pl.treksoft.kvision.utils.obj
import pl.treksoft.kvision.utils.perc
import pl.treksoft.kvision.utils.px
import pl.treksoft.kvision.utils.vh

fun Container.epochChart(epochScore: List<Pair<Double, Double>>) = div { id = "epochchartdiv" }.apply {
            width = 100.perc
            height = 38.perc
            minHeight = 350.px
            afterInsertHook = {
                Dygraph("epochchartdiv", epochScore.map { arrayOf(it.first.withSingleDigit, it.second.withSingleDigit) }.toTypedArray(), obj {
                    // customBars = true
                    labels = arrayOf("Epoch", "Score")
                    connectSeparatedPoints = true
                    titleHeight = 32
                    series = obj {
                        Score = obj {
                            strokeWidth = 2.0
                            this.color = "#007bf7"
                        }
                    }
                })
            }
        }
