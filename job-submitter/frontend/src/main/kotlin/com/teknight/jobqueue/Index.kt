package com.teknight.jobqueue

import com.teknight.jobqueue.api.ApiClient
import com.teknight.jobqueue.redux.RouteState
import com.teknight.jobqueue.redux.store
import pl.treksoft.kvision.core.Container
import pl.treksoft.kvision.html.TAG
import pl.treksoft.kvision.html.div
import pl.treksoft.kvision.html.h1
import pl.treksoft.kvision.html.tag
import pl.treksoft.kvision.state.bind

fun Container.index(routeState: RouteState) {
    col("p-5", "card", "shadow") {
        div(classes = setOf("text-center")) {
            h1("Job submission tool")
        }
        tag(TAG.HR)
        jobForm(routeState.JobPullerStatus)
        div(classes = setOf("m-3"))
        tag(TAG.DIV).bind(store) { state ->
            if (state.jobs.isEmpty()) {
                if (!state.loadingJobs) {
                    ApiClient.getJobs()
                }
                spinner()
            } else {
                jobList(state.jobs, routeState.renderAvailable)
            }
        }
    }
}

