@file:JsModule("regression")
@file:Suppress("INTERFACE_WITH_SUPERCLASS", "OVERRIDING_FINAL_MEMBER", "RETURN_TYPE_MISMATCH_ON_OVERRIDE", "CONFLICTING_OVERLOADS")
package com.teknight.jobqueue

import kotlin.js.*
import kotlin.js.Json
import org.khronos.webgl.*
import org.w3c.dom.*
import org.w3c.dom.events.*
import org.w3c.dom.parsing.*
import org.w3c.dom.svg.*
import org.w3c.dom.url.*
import org.w3c.fetch.*
import org.w3c.files.*
import org.w3c.notifications.*
import org.w3c.performance.*
import org.w3c.workers.*
import org.w3c.xhr.*

external interface Options {
    var precision: Number?
        get() = definedExternally
        set(value) = definedExternally
    var order: Number?
        get() = definedExternally
        set(value) = definedExternally
}

external interface Result {
    var string: String
    var points: Array<dynamic /* JsTuple<Number, Number> */>
    var predict: (x: Number) -> dynamic
    var equation: Array<Number>
    var r2: Number
}

external fun _round(number: Number, precision: Number): Number

external fun linear(data: Array<dynamic /* JsTuple<Number, Number> */>, options: Options = definedExternally): Result

external fun exponential(data: Array<dynamic /* JsTuple<Number, Number> */>, options: Options = definedExternally): Result

external fun logarithmic(data: Array<dynamic /* JsTuple<Number, Number> */>, options: Options = definedExternally): Result

external fun power(data: Array<dynamic /* JsTuple<Number, Number> */>, options: Options = definedExternally): Result

external fun polynomial(data: Array<dynamic /* JsTuple<Number, Number> */>, options: Options = definedExternally): Result
