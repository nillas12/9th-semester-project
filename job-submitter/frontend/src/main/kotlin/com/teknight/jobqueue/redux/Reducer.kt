package com.teknight.jobqueue.redux

fun routeReducer(state: RouteState, action: RouteActions): RouteState = when(action) {
    is RouteActions.LoadJob -> state.copy(selectedJob = action.job)
    is RouteActions.GotoDetailedJob -> state.copy(detailedJobId = action.jobId)
    is RouteActions.SetJobPullerStatus -> state.copy(JobPullerStatus = action.pullerStatus)
    RouteActions.ClearDetailedJob -> state.copy(detailedJobId = null, selectedJob = null)
    RouteActions.EnableRendering -> state.copy(renderAvailable = true)
    RouteActions.ToggleDetailedView -> state.copy(detailedView = !state.detailedView)
}

fun reducer(state: State, action: Actions): State = when (action) {
    is Actions.LoadJobs -> state.copy(jobs = action.jobs, loadingJobs = false)
    Actions.StartLoadingJobs -> state.copy(loadingJobs = true, jobs = emptyList())
}