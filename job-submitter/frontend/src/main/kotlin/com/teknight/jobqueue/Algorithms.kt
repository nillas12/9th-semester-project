package com.teknight.jobqueue

val algorithms = listOf(
        "random" to "Random",
        "dqn" to "Deprecated DQN",
        "dqn_torch" to "DQN (Torch)",
        "d3qn" to "Deprecated D3QN",
        "d3qn_torch" to "D3QN (Torch)",
        "dqfd" to "DQfD",
        "a3c" to "A3C",
        "a2c" to "A2C",
        "hacker_dqn" to "HDQN",
        "hacker_d3qn" to "HD3QN"
)
val algorithmsMap = algorithms.toMap()