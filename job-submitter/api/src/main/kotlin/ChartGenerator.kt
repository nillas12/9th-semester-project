import kotlin.math.roundToInt

fun main() {
    val jobId = "166"
    val epochScores = MongoStore.getEpochScoresForJob(jobId)
    val job = MongoStore.getJob(jobId)
    val algorithm = job.algorithm.removeSuffix("_torch").toUpperCase()
    val game = job.gymEnvironment.removeSuffix("NoFrameskip-v4").map { if (it.isLowerCase()) "$it" else " $it" }.joinToString("").removePrefix(" ")
    createChartFrom(algorithm, game, epochScores)
}

fun generateTicks(max: Double, totalTicks: Int = 10, min: Double = 0.0): List<Int> {
    val stepSize = (max / totalTicks).roundToInt()
    val listNoMax = listOf(min.roundToInt()) + (1 until totalTicks).map { (min + it * stepSize).roundToInt() }
    return listNoMax + if (listNoMax.last() < max) listOf(max.toInt()) else emptyList()
}

fun Double.format(digits: Int) = "%.${digits}f".format(this)
fun formatNum(score: Int) = when {
    score > 99999 -> "${(score / 1000.0).format(0)}k"
    score > 9999 -> "${(score / 1000.0).format(1)}k"
    score > 999 -> "${(score / 1000.0).format(2)}k"
    else -> "$score"
}

fun createChartFrom(game: String, algorithm: String, epochScores: List<EpochScore>) {
    val maxX = epochScores.last().epoch
    val maxY = epochScores.maxByOrNull { it.score }?.score ?: 0.0
    val minY = epochScores.minByOrNull { it.score }?.score ?: 0.0
    // 123  0 120 step=20 +append 123

    val ticksX = 10
    val ticksY = 6
    val xTick = generateTicks(maxX, ticksX)
    val yTick = generateTicks(maxY, ticksY, minY)

    println(
        """
        \begin{figure}[H]
        \centering
        \caption{Evaluation of $algorithm performance for $game}
        \label{fig:${algorithm.toLowerCase()}_${game.toLowerCase()}}
        \makebox[\textwidth][c]{
        \begin{tikzpicture}
        \begin{axis}[
            xlabel={Epoch},
            ylabel={Environment Score},
            xmin=0, xmax=$maxX,
            ymin=$minY, ymax=$maxY,
            xtick={${xTick.joinToString(",")}},
            ytick={${yTick.joinToString(",")}},
            yticklabels={${yTick.joinToString(",") { formatNum(it) }}},
            scaled y ticks=false,
            legend pos=north west,
            ymajorgrids=true,
            grid style=dashed,
        ]
        \addplot[
            color=myBlue,
            line width=0.8pt
            ]
            coordinates {
    """.trimIndent()
    )
    epochScores.forEach {
        val epoch = it.epoch
        val score = it.score
        print("($epoch, $score)")
    }
    println("""
        };
        \addplot[
            color=myRed,
            line width=1pt
            ]
            coordinates {
    """.trimIndent()
    )
    epochScores.reversed().windowed(15, 1, true) { it.first().epoch to it.sumByDouble { it.score } / 15 }.forEach {
        val epoch = it.first
        val score = it.second
        print("($epoch, $score)")
    }
    println("""

        };
        \legend{Evaluation score, Sliding average (15 points)}

        \end{axis}
        \end{tikzpicture}
        }
        \end{figure}
    """.trimIndent()
    )
}

