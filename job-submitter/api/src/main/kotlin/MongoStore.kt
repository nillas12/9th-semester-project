import com.mongodb.ConnectionString
import org.litote.kmongo.*
import java.time.Instant

object MongoStore {
    val client = KMongo.createClient(ConnectionString("mongodb://hcl:HclPassword123@40.113.40.87/admin?retryWrites=true&w=majority"))
    private val database = client.getDatabase("hcljqs")
    private val jobs = database.getCollection<JobShort>()
    private val pullStatus = database.getCollection<LastPull>()
    private val scores = database.getCollection<MetricDataAcc>()
    private val logs = database.getCollection<LogDataAcc>()
    private val epochScores = database.getCollection<EpochScoreWithId>()

    fun addJob(job: JobShort) {
        val id = (jobs.find().toList().maxByOrNull { it.jobId.toInt() }?.jobId?.toInt() ?: 0) + 1
        jobs.insertOne(job.copy(jobId = "$id"))
    }

    fun getJobs(all: Boolean) = jobs.find().sortedByDescending { it.jobId.toInt() }.take(if (all) 9999 else 20).toList()
    fun getJob(id: String) = jobs.findOne { JobShort::jobId eq id }!!
    fun updateJob(job: JobShort) {
        jobs.updateOne(JobShort::jobId eq job.jobId, job)
    }
    fun addScore(jobId: String, metric: ScoreWithTs) {
        scores.updateOne(MetricDataAcc::jobId eq jobId, push(MetricDataAcc::scores, metric), upsert())
    }
    fun getScoreForJob(id: String) = scores.findOne(MetricDataAcc::jobId eq id)?.scores ?: emptyList()
    fun updatePullStatus(host: String, isRunning: Boolean) {
        pullStatus.replaceOne(LastPull::host eq host, LastPull(Instant.now().epochSecond, host, isRunning), replaceUpsert())
    }
    fun getPullStatus() = pullStatus.find().toList()
    fun addLog(jobId: String, log: LogWithTs) {
        logs.updateOne(LogDataAcc::jobId eq jobId, push(LogDataAcc::logs, log), upsert())
    }
    fun getLogsForJob(id: String) = logs.findOne(LogDataAcc::jobId eq id)?.logs ?: emptyList()
    fun addEpochScore(id: String, epochScore: EpochScore) {
        epochScores.updateOne(EpochScoreWithId::jobId eq id, push(EpochScoreWithId::epochScore, epochScore), upsert())
    }
    fun getEpochScoresForJob(id: String) = epochScores.findOne(EpochScoreWithId::jobId eq id)?.epochScore ?: emptyList()
}