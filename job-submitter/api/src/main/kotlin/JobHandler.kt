import java.text.SimpleDateFormat
import java.time.Instant
import java.util.*

fun createJob(job: JobCreation): List<JobShort> {
    val shortJob = JobShort(
        jobId = "",
        lastEpisodeEnded = "-",
        progress = 0,
        gymEnvironment = job.env,
        eta = "-",
        jobStatus = JobStatus.Pending,
        trainingEpisodes = job.trainingEpisodes,
        algorithm = job.algorithm,
        dockerImageVersion = job.dockerImage,
        episodeDuration = "-",
        host = ""
    )
    MongoStore.addJob(shortJob)
    return loadAllJobs(false)
}
fun loadAllJobs(all: Boolean = false) = MongoStore.getJobs(all)

fun loadJob(id: String, detailed: Boolean): JobLong {
    val job = MongoStore.getJob(id)
    val score = MongoStore.getScoreForJob(id)
    val filteredScore = if (detailed) score.mapIndexed { index, it -> index to it.score } else {
        val maxShow = 5000
        val stepSize = (score.size - 1).toDouble() / (maxShow - 1).toDouble()
        score.foldIndexed(Pair<Double, Map<Int, Double>>(0.0, emptyMap())) { index, it, s ->
            if (it.first <= index) {
                it.copy(first = it.first + stepSize, second = it.second + (index to s.score))
            } else {
                it
            }
        }.second.toList()
    }
    val logs = MongoStore.getLogsForJob(id).map { it.ts to it.message }
    val epochScore = MongoStore.getEpochScoresForJob(id).map { it.epoch to it.score }
    return JobLong(job, filteredScore, logs, epochScore)
}

fun updateJob(id: String, metric: Metric) {
    val job = MongoStore.getJob(id)
    if (job.jobStatus == JobStatus.Canceled || job.jobStatus == JobStatus.Canceling) {
        return
    }
    val pattern = "MM-dd HH:mm"
    val simpleDateFormat = SimpleDateFormat(pattern).apply {
        timeZone = TimeZone.getTimeZone("CET")
    }
    val newJob = if (metric.progress < 100.0 && job.jobStatus != JobStatus.Completed) {
        job.copy(
            jobStatus = JobStatus.Running,
            eta = if (metric.eta == -1) "Estimating..." else simpleDateFormat.format(Date.from(Instant.now().plusSeconds(metric.eta.toLong()))),
            episodeDuration = (metric.episodeDuration).toInt().toString() + "s",
            progress = metric.progress.toInt(),
            lastEpisodeEnded = simpleDateFormat.format(Date.from(Instant.now()))
        )
    } else {
        job.copy(
            jobStatus = JobStatus.Completed,
            eta = "-",
            progress = 100,
        )
    }
    MongoStore.addScore(id, ScoreWithTs(metric.lastScore, Instant.now().epochSecond))
    MongoStore.updateJob(newJob)
}

fun cancelJob(id: String): List<JobShort> {
    val job = MongoStore.getJob(id)
    val newJob = job.copy(jobStatus = if (job.jobStatus == JobStatus.Pending) JobStatus.Canceled else JobStatus.Canceling, eta = "-")
    addLog(id, Log("User submitted cancel request"))
    MongoStore.updateJob(newJob)
    return MongoStore.getJobs(false)
}

fun addLog(id: String, log: Log) {
    val job = MongoStore.getJob(id)
    if (log.message.contains("ERROR")) {
        val newJob = job.copy(jobStatus = JobStatus.Failed, eta = "-")
        MongoStore.updateJob(newJob)
    }
    val pattern = "MM-dd HH:mm"
    val simpleDateFormat = SimpleDateFormat(pattern).apply {
        timeZone = TimeZone.getTimeZone("CET")
    }
    MongoStore.addLog(id, LogWithTs(log.message, simpleDateFormat.format(Date.from(Instant.now()))))
}

fun addEpochScore(id: String, epochScore: EpochScore) {
    MongoStore.addEpochScore(id, epochScore)
    val job = MongoStore.getJob(id)
    if (job.trainingEpisodes.toDouble() <= epochScore.epoch + 0.01) {
       MongoStore.updateJob(job.copy(
            jobStatus = JobStatus.Completed,
            eta = "-",
            progress = 100,
        ))
    }
}

fun continueJob(id: String, totalEpochs: Int) {
    val pattern = "MM-dd HH:mm"
    val simpleDateFormat = SimpleDateFormat(pattern).apply {
        timeZone = TimeZone.getTimeZone("CET")
    }
    val job = MongoStore.getJob(id)
    val scores = MongoStore.getScoreForJob(id)
    updateJob(id, Metric(job.progress.toDouble(), scores.last().score, -1, 0.0))
    MongoStore.updateJob(job.copy(
        trainingEpisodes = totalEpochs,
        jobStatus = JobStatus.Pending
    ))
    MongoStore.addLog(id, LogWithTs("Continuing from last checkpoint", simpleDateFormat.format(Date.from(Instant.now()))))
}