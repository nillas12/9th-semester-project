import java.time.Instant

fun pullRequest(request: PullRequest): String {
    MongoStore.updatePullStatus(request.host, !request.readyToPull)
    return if (request.readyToPull) {
        val oldJob = MongoStore.getJobs(true).sortedBy { it.jobId }.firstOrNull { it.host == request.host && (it.jobStatus == JobStatus.Running || it.jobStatus == JobStatus.Initializing) }
        if (oldJob != null) {
            val newJob = oldJob.copy(jobStatus = JobStatus.Failed, eta = "-")
            MongoStore.updateJob(newJob)
            addLog(oldJob.jobId, Log("Job failed since host reported ready for job while the current job should be running."))
        }
        val job = MongoStore.getJobs(true).sortedBy { it.jobId }.firstOrNull { it.jobStatus == JobStatus.Pending }
        if (job != null) {
            val newJob = job.copy(host = request.host, jobStatus = JobStatus.Initializing)
            MongoStore.updateJob(newJob)
            Thread.sleep(5000)
            val updatedJob = MongoStore.getJob(newJob.jobId)
            if (updatedJob.host == request.host) {
                val epochs = MongoStore.getEpochScoresForJob(updatedJob.jobId).lastOrNull()?.epoch?.toInt() ?: 0
                if (epochs == 0) {
                    addLog(job.jobId, Log("Job accepted by host. Waiting for host to initialize environment."))
                    "salloc -N1 -n1 --gres=gpu:1 --mem=64G srun singularity run --nv docker://registry.gitlab.com/nillas12/9th-semester-project:${job.dockerImageVersion} ${job.gymEnvironment} ${job.algorithm} ${job.trainingEpisodes} ${job.jobId} </dev/null &"
                } else {
                    addLog(job.jobId, Log("Continued job accepted by host. Waiting for host to initialize environment."))
                    "salloc -N1 -n1 --gres=gpu:1 --mem=64G srun singularity run --nv docker://registry.gitlab.com/nillas12/9th-semester-project:${job.dockerImageVersion} ${job.gymEnvironment} ${job.algorithm} ${job.trainingEpisodes} ${job.jobId} $epochs </dev/null &"
                }
            } else {
                "echo Did not get new job, checking for next later"
            }
        } else {
            "echo no jobs"
        }
    } else {
        val currentJob = MongoStore.getJobs(true).firstOrNull { (it.jobStatus == JobStatus.Running || it.jobStatus == JobStatus.Canceling) && it.host == request.host }
        if (currentJob != null) {
            if (currentJob.jobStatus == JobStatus.Canceling) {
                val newJob = currentJob.copy(jobStatus = JobStatus.Canceled, eta = "-")
                addLog(currentJob.jobId, Log("Job cancel send to server"))
                MongoStore.updateJob(newJob)
                "squeue -u \$(whoami) | sed 1d | awk '{print \$1}' | xargs -n 1 scancel"
            } else {
                val scores = MongoStore.getScoreForJob(currentJob.jobId)
                val shouldTerminate = Instant.now().minusSeconds(3600).epochSecond > scores.last().ts
                if (shouldTerminate) {
                    val newJob = currentJob.copy(jobStatus = JobStatus.Failed, eta = "-")
                    addLog(currentJob.jobId, Log("Failing job since time since LEE is > 60min"))
                    MongoStore.updateJob(newJob)
                    "squeue -u \$(whoami) | sed 1d | awk '{print \$1}' | xargs -n 1 scancel"
                } else {
                    "echo You are alive, running a healthy job!"
                }
            }
        } else {
            "echo You are alive, hooray!"
        }
    }
}

fun pullStatus(): List<PullStatus> {
    return MongoStore.getPullStatus().map {
        PullStatus(if ((Instant.now().epochSecond - it.lastSeen) < 120) {
            if (it.isRunning) PullerStatus.Running else PullerStatus.Ready
        } else PullerStatus.Down, it.host)
    }
}