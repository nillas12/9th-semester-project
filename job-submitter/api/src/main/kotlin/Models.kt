enum class JobStatus {
    Pending,
    Running,
    Completed,
    Failed,
    Initializing,
    Canceling,
    Canceled
}

data class JobShort(
    val jobId: String,
    val lastEpisodeEnded: String,
    val progress: Int,
    val gymEnvironment: String,
    val eta: String,
    val jobStatus: JobStatus,
    val trainingEpisodes: Int,
    val algorithm: String,
    val dockerImageVersion: String,
    val episodeDuration: String,
    val host: String
)

enum class PullerStatus {
    Ready,
    Running,
    Down
}

data class JobLong(val job: JobShort, val score: List<Pair<Int, Double>>, val logs: List<Pair<String, String>>, val epochScore: List<Pair<Double, Double>>)

data class ScoreWithTs(val score: Double, val ts: Long)
data class MetricDataAcc(val jobId: String, val scores: List<ScoreWithTs>)
data class LogWithTs(val message: String, val ts: String)
data class LogDataAcc(val jobId: String, val logs: List<LogWithTs>)
data class EpochScoreWithId(val jobId: String, val epochScore: List<EpochScore>)

data class JobCreation(val env: String, val trainingEpisodes: Int, val algorithm: String, val dockerImage: String)
data class JobResume(val jobId: String, val epochs: Int)

data class PullRequest(val readyToPull: Boolean, val host: String)
data class LastPull(val lastSeen: Long, val host: String, val isRunning: Boolean)
data class PullStatus(val pullStatus: PullerStatus, val host: String)


data class EpochScore(val epoch: Double, val score: Double)
data class Metric(val progress: Double, val lastScore: Double, val eta: Int, val episodeDuration: Double)
data class Log(val message: String)