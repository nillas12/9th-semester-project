package TimeGenerator

import ScoreWithTs
import org.nield.kotlinstatistics.*
import kotlin.math.roundToInt
import kotlin.math.sqrt

fun main() {
    val jobIds = listOf("114", "166", "170")
    val scores = jobIds.map { MongoStore.getScoreForJob(it) }
    val jobs = jobIds.map { MongoStore.getJob(it) }
    require(jobs.map { it.gymEnvironment }.toSet().size == 1)
    val algorithms = jobs.map { it.algorithm.removeSuffix("_torch").toUpperCase() }
    val game = jobs.first().gymEnvironment.removeSuffix("NoFrameskip-v4").map { if (it.isLowerCase()) "$it" else " $it" }.joinToString("").removePrefix(" ")
    createChartFrom(jobIds, game, algorithms, scores, 48.0)
}

fun generateTicks(max: Double, totalTicks: Int = 10, min: Double = 0.0): List<Int> {
    val stepSize = (max / totalTicks).roundToInt()
    val listNoMax = listOf(min.roundToInt()) + (1 until totalTicks).map { (min + it * stepSize).roundToInt() }
    return listNoMax + if (listNoMax.last() < max) listOf(max.toInt()) else emptyList()
}

fun Double.format(digits: Int) = "%.${digits}f".format(this)
fun formatNum(score: Int) = when {
    score > 99999 -> "${(score / 1000.0).format(0)}k"
    score > 9999 -> "${(score / 1000.0).format(1)}k"
    score > 999 -> "${(score / 1000.0).format(2)}k"
    else -> "$score"
}

val colors = listOf("myBlue", "myRed", "myGreen")

data class Plot(val x: Double, val y: Double, val confidenceHigh: Double, val confidenceLow: Double)

fun createChartFrom(jobIds: List<String>, game: String, algorithms: List<String>, scores: List<List<ScoreWithTs>>, hours: Double) {
    val plots = scores.map {
        val start = it.first().ts
        it.map {
            ScoreWithTs(it.score, it.ts - start)
        }.groupBy {
            ((it.ts * 2) / 3600.0).toInt() / 2.0
        }.toList().sortedBy { it.first }.take( (hours * 2).toInt() ).mapIndexed { index, it ->
            index / 2.0 to it.second
        }.map {
            it.first to it.second.map { it.score }
        }.map {
            val average = it.second.average()
            val stdDeviation = it.second.standardDeviation()
            val confidenceFactor = 1.96 // Confidence level 95%
            val marginError = (stdDeviation * confidenceFactor) / sqrt(it.second.size.toDouble())
            Plot(it.first, average, average + marginError, average - marginError)
        }
    }
    val maxY = plots.map { it.maxByOrNull { it.confidenceHigh } }.maxByOrNull { it?.confidenceHigh ?: 0.0 }?.confidenceHigh ?: 0.0
    val minY = plots.map { it.minByOrNull { it.y } }.minByOrNull { it?.y ?: 0.0 }?.y ?: 0.0
    // 123  0 120 step=20 +append 123

    val ticksX = 12
    val ticksY = 6
    val xTick = generateTicks(hours, ticksX)
    val yTick = generateTicks(maxY, ticksY, minY)

    println(
        """
        % Job ID's: ${jobIds.joinToString(", ")}
        \begin{figure}[H]
        \centering
        \caption{Comparison of ${algorithms.joinToString { it }} learning over ${hours.toInt()} hours}
        \label{fig:time_compare_${game.toLowerCase()}}
        \makebox[\textwidth][c]{
        \begin{tikzpicture}
        \begin{axis}[
            xlabel={Hour},
            ylabel={Environment Score},
            xmin=0, xmax=$hours,
            ymin=$minY, ymax=$maxY,
            xtick={${xTick.joinToString(",")}},
            ytick={${yTick.joinToString(",")}},
            yticklabels={${yTick.joinToString(",") { formatNum(it) }}},
            scaled y ticks=false,
            legend pos=north west,
            ymajorgrids=true,
            grid style = {dashed, draw=gray!50},
            axis background/.style={fill=gray!5},
        ]
        """.trimIndent()
    )
    plots.forEachIndexed { index, it ->
        println("""
            \addplot[
                stack plots=y,
                fill=none,
                draw=none,
                forget plot,
            ]
            coordinates {
        """.trimIndent()
        )
        it.forEach {
            val x = it.x.format(1)
            val y = it.confidenceLow.format(1)
            print("($x, $y)")
        }
        println("""
            } \closedcycle;
        """.trimIndent())
        println("""
            \addplot[
                stack plots=y,
                fill=${colors[index]}, 
                opacity=0.2, 
                draw opacity=0,
                area legend
            ]
            coordinates {
        """.trimIndent()
        )
        it.forEach {
            val x = it.x.format(1)
            val y = (it.confidenceHigh - it.confidenceLow).format(1)
            print("($x, $y)")
        }
        println("""
            } \closedcycle;
        """.trimIndent())
        println("""
            \addplot[
                stack plots=y,
                stack dir=minus,
                draw=none,
                forget plot,
            ]
            coordinates {
        """.trimIndent()
        )
        it.forEach {
            val x = it.x.format(1)
            val y = it.confidenceHigh.format(1)
            print("($x, $y)")
        }
        println("""
            };
        """.trimIndent())
        println("""
            \addplot[
                color=${colors[index]},
                line width=1pt
            ]
            coordinates {
        """.trimIndent()
        )
        it.forEach {
            val hour = it.x.format(1)
            val score = it.y.format(1)
            print("($hour, $score)")
        }
        println("""
            };
        """.trimIndent())
    }
    println("""
        \legend{${algorithms.joinToString(", ") { ",$it" }}}
        \end{axis}
        \end{tikzpicture}
        }
        \end{figure}
    """.trimIndent()
    )
}

