import io.javalin.Javalin

fun main() {
    val app = Javalin.create {
        it.enableCorsForAllOrigins()
    }.start(7000)
    app.get("/job") { ctx -> ctx.json(loadAllJobs()) }
    app.get("detailed/job") { ctx -> ctx.json(loadAllJobs(true)) }
    app.post("/job") { ctx ->
        val c = ctx.body<JobCreation>()
        ctx.json(createJob(c))
    }
    app.post("/job/resume") { ctx ->
        val c = ctx.body<JobResume>()
        ctx.json(continueJob(c.jobId, c.epochs))
    }
    app.get("/job/:jobId") { ctx ->
        val id = ctx.pathParam("jobId")
        val detailed = ctx.queryParam("detailed", "false").toBoolean()
        ctx.json(loadJob(id, detailed))
    }
    app.delete("/job/:jobId") { ctx ->
        val id = ctx.pathParam("jobId")
        ctx.json(cancelJob(id))
    }
    app.put("/job/:jobId") { ctx ->
        val id = ctx.pathParam("jobId")
        val metric = ctx.body<Metric>()
        ctx.json(updateJob(id, metric))
    }
    app.put("/job/:jobId/epoch") { ctx ->
        val id = ctx.pathParam("jobId")
        val epochScore = ctx.body<EpochScore>()
        ctx.json(addEpochScore(id, epochScore))
    }
    app.put("/job/:jobId/log") { ctx ->
        val id = ctx.pathParam("jobId")
        val log = ctx.body<Log>()
        ctx.json(addLog(id, log))
    }
    app.post("/pull") { ctx ->
        val pr = ctx.body<PullRequest>()
        ctx.result(pullRequest(pr))
    }
    app.get("/pull") { ctx -> ctx.json(pullStatus()) }
}