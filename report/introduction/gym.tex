\section{OpenAI Gym}
OpenAI Gym\cite{openaigympaper} is a toolkit designed for experimentation and development of reinforcement learning models and algorithms.
It provides a suite of tools and benchmark problems, that allow machine learning scientists and hobbyists to easily try their ideas and compare their algorithms.

Gym includes a diverse collection of environments in which the agent can act.
These environments are designed using a common interface where OpenAI Gym handles the states and the rewards, meaning the implementation can focus solely on picking the appropriate action.

OpenAI Gym operates using the episodic setting of reinforcement learning, where the agent's experience is broken down into a series of \textit{episodes}.
In each episode the agent's initial state is randomly sampled from a distribution\footnote{In \ac{ALE} and other third-party environments, whether to use random sampling is up to the individual implementation.} and iterates over a series of timesteps. 
Each episode continues until it reaches a terminal state, or until it reaches the bounded amount of timesteps.

\begin{lstlisting}[language=Python, caption={A simple example of the usage of OpenAI Gym. This example showcases a random agent performing 1 episode bounded to 1000 timesteps.}]
import gym
env = gym.make('CartPole-v0')
env.reset()
for _ in range(1000):
    env.render()
    env.step(env.action_space.sample()) # take a random action
env.close()
\end{lstlisting}

Along with the software library, OpenAI Gym has a website with an up-to-date scoreboard for each environment in the library. 
Scores are submitted by users, often together with the agents' source code and detailed instructions of how to reproduce the agents. 
With this website and continuous user-participation, OpenAI Gym has created ample opportunity for \ac{RL} research and comparing. 

\subsection{Environments}
OpenAI Gym comes bundled with a number of learning environments, spanning a large subset of differing tasks, providing vastly different problems to solve.
The challenge is to create an agent that can successfully navigate all of the environments, without modifying the algorithm, network architecture or hyper-parameters.

OpenAI Gym integrates a number of environments such as MuJoCo, Robotics, Algorithmic, Classic Control and Atari.

\textbf{Atari Learning Environment} \\
\ac{ALE}\cite{bellemare13arcade} is an implementation of over 50 games, fully implemented in C\texttt{++} and usable with simple Python code.
\ac{ALE} is built on top of Stella \footnote{https://stella-emu.github.io/}, an open-source Atari emulator. 
This emulator enables the user to interface with the Atari 2600 by receiving input, sending screen and/or RAM information, and emulating the platform. 
\ac{ALE} also provides a game-handling layer specifically for \ac{RL}, including identifying the accumulated score and whether or not the game has ended.

For this report we restrain the algorithms to train and evaluate solely on the environments provided by the \ac{ALE}.
This decision is made as all algorithms included in this paper were developed primarily using the \ac{ALE}, as well as a measure to constrain the scope of this paper.